package com.jxtech.saber.test.common;

import com.jxtech.saber.test.SmallTestConf;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by cook on 2018/9/26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SmallTestConf.class)
public class BaseServiceTestCase {



}
