package com.jxtech.saber.test;

import com.jxtech.saber.model.dto.sys.TestJustResponse;
import com.jxtech.saber.properties.ProjectConfig;
import com.jxtech.saber.service.bizs.sys.TestJustBiz;
import com.jxtech.saber.test.common.BaseServiceTestCase;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by cook on 2019/3/11
 */
public class JustBizTest extends BaseServiceTestCase {

    @Autowired
    private TestJustBiz testBiz;

    @Autowired
    private ProjectConfig config;

    @Test
    public void test01() {
        List<TestJustResponse> ret = testBiz.findJust(12);
        Assert.assertEquals(ret.size(), 1);

        Assert.assertEquals(config.getJustValue(), 100);
    }



}
