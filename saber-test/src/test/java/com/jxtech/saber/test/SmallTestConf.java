package com.jxtech.saber.test;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * Created by cook on 2017/6/8.
 */
@SpringBootApplication(scanBasePackages="com.jxtech.saber")
@PropertySource({"classpath:${spring.profiles.active}/application.properties", "classpath:application.properties"})
@SpringBootConfiguration
public class SmallTestConf {

}
