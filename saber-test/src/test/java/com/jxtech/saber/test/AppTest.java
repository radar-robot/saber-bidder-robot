package com.jxtech.saber.test;

import java.util.UUID;

/**
 * Created by cook on 2019/5/11
 */
public class AppTest {


    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());

        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);

        System.out.println(Integer.valueOf("10111101", 2));

        // 189
        // 127
        // 61

        // 67
        // 194
        // System.out.println(Byte.valueOf("10111101", 2));

        System.out.println(Byte.parseByte("00111101", 2));

        byte b1 = -95;
        byte i1 = -95;
        System.out.println(Integer.toBinaryString(b1));
        System.out.println(Integer.toBinaryString(i1));

    }

}
