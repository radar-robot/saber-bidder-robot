package com.jxtech.saber.service.bizs.sys;

import com.jxtech.saber.model.dto.sys.TestJustResponse;
import com.jxtech.saber.service.services.sys.TestJustService;
import com.jxtech.saber.support.utils.BizAssert;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cook on 2019/1/25
 */
@Component
public class TestJustBiz {

    @Autowired
    private TestJustService testService;

    /**
     *
     * @param userId
     * @return
     */
    public List<TestJustResponse> findJust(Integer userId) {
        BizAssert.isTrue(userId != null && userId > 0, "无效的用户id");

        return Lists.newArrayList(testService.getOneJust());
    }

}
