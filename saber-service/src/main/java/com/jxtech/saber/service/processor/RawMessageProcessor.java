package com.jxtech.saber.service.processor;

import com.jxtech.saber.support.common.RawMessageType;
import com.jxtech.saber.support.message.RawMessage;

/**
 * Created by cook on 2019/3/30
 */
public interface RawMessageProcessor {

    /**
     * 根据 <code>request</code>, 处理
     * @param request
     * @return response
     */
    RawMessage process(RawMessage request);

    /**
     *
     * @return
     */
    RawMessageType messageType();



}
