package com.jxtech.saber.service.socket.netty;

import com.jxtech.saber.properties.ProjectConfig;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/3/30
 */
@Component
public class NettySocketServer {

    private static final Logger logger = LoggerFactory.getLogger(NettySocketServer.class);

    private boolean isRunning;

    @Autowired
    private SaberServerInitializer saberServerInitializer;

    @Autowired
    private ProjectConfig conf;

    public void start() {
        start(conf.getSocketServerPort());
    }


    public void start(int port) {
        logger.info("begin run saber-server on port#{}", port);

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b = b.option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true);
            // bootstrap.setOption("reuseAddress", true);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(saberServerInitializer)
            ;

            ChannelFuture bindFuture = b.bind(port);

            bindFuture.addListener(future -> logger.info("bind done"));

            ChannelFuture syncFuture = bindFuture.sync();

            syncFuture.addListener(future -> {
                logger.info("syn done");
                isRunning = true;
            });

            ChannelFuture closeFuture = syncFuture.channel().closeFuture();

            closeFuture.addListener(future -> logger.info("close done"));

            Runtime.getRuntime().removeShutdownHook(new Thread(() -> logger.info("shutdown...")));

            logger.info("before close future sync");

            closeFuture.sync();

        } catch (InterruptedException e) {
            throw new RuntimeException("", e);
        } finally {
            logger.info("shutdown saber-server");
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }

}
