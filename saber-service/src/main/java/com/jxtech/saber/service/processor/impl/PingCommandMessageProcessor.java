package com.jxtech.saber.service.processor.impl;

import com.jxtech.saber.service.processor.BaseRawMessageChannelProcessor;
import com.jxtech.saber.support.common.RawMessageType;
import com.jxtech.saber.support.message.RawMessage;
import com.jxtech.saber.support.utils.RawMessages;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/10/26
 */
@Component
public class PingCommandMessageProcessor extends BaseRawMessageChannelProcessor {

    private static final Logger logger = LoggerFactory.getLogger(PingCommandMessageProcessor.class);

    @Override
    public RawMessage process(ChannelHandlerContext ctx, RawMessage msg) {
        return RawMessages.from(RawMessageType.PING_COMMAND, msg.getClientNo(), "pong");
    }

    @Override
    public RawMessageType messageType() {
        return RawMessageType.PING_COMMAND;
    }

}
