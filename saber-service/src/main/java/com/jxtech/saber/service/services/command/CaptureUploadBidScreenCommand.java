package com.jxtech.saber.service.services.command;

import com.jxtech.saber.model.dto.sys.BidderRegisterRequest;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.service.bizs.sys.BidderChannelManager;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.support.command.BaseCommand;
import com.jxtech.saber.support.message.JsonCommand;
import com.jxtech.saber.support.utils.JsonCommands;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/5/26
 */
@Component
public class CaptureUploadBidScreenCommand extends BaseCommand<BidderRegisterRequest> {

    @Autowired
    private BidderChannelManager channelManager;

    @Autowired
    private RadarClientManager userManager;

    @Override
    public JsonCommand doExecute(ChannelHandlerContext ctx, int clintNo, BidderRegisterRequest req) {

        return null;
    }

    @Override
    public CommandDirective getDirective() {
        return CommandDirective.CAPTURE_UPLOAD_BID_SCREEN;
    }

}
