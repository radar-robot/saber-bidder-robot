//package com.jxtech.saber.service.cache;
//
//import com.fxtech.panda.cache.guava.BaseGuavaLocalCache;
//import org.springframework.stereotype.Component;
//
///**
// * 15分钟过期时间 - 简单对象本地缓存
// * Created by cook on 2019/1/11
// */
//@Component
//public class FifteenMinutesLocalCache extends BaseGuavaLocalCache {
//
//    @Override
//    public long getTimeoutMills() {
//        return 15 * 60 * 1000;
//    }
//
//    @Override
//    public long getMaxSize() {
//        return 50 * 1000;
//    }
//}
