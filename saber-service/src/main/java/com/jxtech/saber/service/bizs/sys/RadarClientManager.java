package com.jxtech.saber.service.bizs.sys;

import com.fxtech.panda.core.json.Jack;
import com.google.common.collect.Interner;
import com.google.common.collect.Interners;
import com.google.common.collect.Maps;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.model.dto.sys.BidderRegisterRequest;
import com.jxtech.saber.model.enums.BidderStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by cook on 2019/3/31
 */
@Component
public class RadarClientManager {

    private static final Logger logger = LoggerFactory.getLogger(RadarClientManager.class);

    private static final Interner<String> lock = Interners.newWeakInterner();


    private static final Map<String, BidderClient> all = Maps.newConcurrentMap();
    private static final Map<Integer, BidderClient> all2 = Maps.newConcurrentMap();

    private static final Map<String, BidderClient> workings = Maps.newConcurrentMap();

    private static final AtomicInteger sequence = new AtomicInteger(12);

    // 缓存 machine-code -- clientNo 关系
    private static final Map<String, Integer> machineNos = Maps.newConcurrentMap();

    public List<BidderClient> queryAllBidders() {
        return new ArrayList<>(workings.values());
    }

    /**
     *
     * @return
     */
    public List<BidderClient> queryWorkingClients() {
        return workings.values().stream()
                .filter(x -> x.getStatus() == BidderStatus.WORKING)
                .collect(Collectors.toList());
    }

    public BidderClient getByClientNo(int clientNo) {
        return all2.get(clientNo);
    }

    public BidderClient getByMachineCode(String machineCode) {
        int cNo = machineNos.get(machineCode);
        return all2.get(cNo);
    }

    /**
     *
     * @param req
     * @return
     */
    public BidderClient register(BidderRegisterRequest req) {
        logger.info("user-request#{} register now", Jack.toJson(req));

        String bidderId = req.getMachineCode();
        if (StringUtils.isEmpty(bidderId)) {
            return null;
        }

        BidderClient bidder = all.get(bidderId);
        synchronized (lock.intern(bidderId+"Login")) {
            if (bidder == null) {
                bidder = new BidderClient();
                bidder.setMachineCode(bidderId);
                bidder.setMachineAddress(req.getMachineAddress());

                int no = getOrAssignClientNo(bidderId);

                bidder.setAssignedClientNo(no);

                all.put(bidderId, bidder);
                all2.put(no, bidder);
            }
        }


        bidder.setStatus(BidderStatus.WORKING);
        bidder.setLastLoginTime(LocalDateTime.now());

        workings.put(bidderId, bidder);


        return bidder;
    }



    public BidderClient disconnect(String bidderId, boolean isTransportError) {
        if (StringUtils.isEmpty(bidderId)) {
            return null;
        }

        BidderClient bidder = all.get(bidderId);

        if (bidder == null) {

            return null;
        }

        logger.info("logout bidder#{}", bidderId);
        bidder.setStatus(BidderStatus.OFFLINE);
        bidder.setLastDisconnectedTime(LocalDateTime.now());

        if (isTransportError) {
            bidder.setLastTransportErrorTime(LocalDateTime.now());
        }

        workings.remove(bidderId);

        return bidder;
    }

    public int getOrAssignClientNo(String machineCode) {
        if (StringUtils.isEmpty(machineCode)) {
            return -1;
        }

        Integer clientNo = machineNos.get(machineCode);
        if (clientNo != null) {
            return clientNo;
        }

        synchronized (lock.intern(machineCode)) {
            clientNo = machineNos.get(machineCode);
            if (clientNo != null) {
                return clientNo;
            }

            clientNo = sequence.incrementAndGet();
            machineNos.put(machineCode, clientNo);
        }

        return clientNo;
    }


}
