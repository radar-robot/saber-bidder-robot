package com.jxtech.saber.service.services.sys;

import com.google.common.collect.Maps;
import com.jxtech.saber.model.bo.AccountSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by cook on 2019/10/25
 */
@Service
public class AccountSettingService {

    private static final Logger logger = LoggerFactory.getLogger(AccountSettingService.class);

    private Map<String, AccountSetting> store = Maps.newHashMap();

    public void addAccountSetting(AccountSetting setting) {
        store.put(setting.getMachineCode(), setting);
        logger.info("add account#{}, setting#{}", setting.getMachineCode(), setting.toText());
    }

    public AccountSetting getByMachineCode(String machineCode) {
        return store.get(machineCode);
    }

}
