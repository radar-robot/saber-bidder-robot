package com.jxtech.saber.service.socket.netty;

import com.jxtech.saber.support.utils.K;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * Created by cook on 2019/9/14
 */
@Getter
public class BidderChannelContext {

    private String bidderId;

    private String channelId;

    private ChannelHandlerContext context;

    private LocalDateTime connectTime;

    public BidderChannelContext(ChannelHandlerContext context) {
        this.context = context;
        this.channelId = K.getChannelId(context);
    }

    public void setBidderId(String bidderId) {
        this.bidderId = bidderId;
    }

    public void setConnectTime(LocalDateTime connectTime) {
        this.connectTime = connectTime;
    }

}
