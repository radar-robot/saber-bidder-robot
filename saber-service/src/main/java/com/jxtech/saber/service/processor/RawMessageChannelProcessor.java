package com.jxtech.saber.service.processor;

import com.jxtech.saber.support.common.RawMessageType;
import com.jxtech.saber.support.message.RawMessage;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by cook on 2019/3/30
 */
public interface RawMessageChannelProcessor extends RawMessageProcessor {

    /**
     * 根据 <code>request</code>, 处理
     * @param ctx
     * @param message
     * @return
     */
    RawMessage process(ChannelHandlerContext ctx, RawMessage message);

    /**
     *
     * @return
     */
    RawMessageType messageType();



}
