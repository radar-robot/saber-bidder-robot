package com.jxtech.saber.service.processor;

import com.jxtech.saber.support.message.RawMessage;

/**
 * Created by cook on 2019/3/31
 */
public abstract class BaseRawMessageChannelProcessor extends BaseRawMessageProcessor implements RawMessageChannelProcessor {

    @Override
    public RawMessage process(RawMessage message) {
        throw new RuntimeException("unsupported method");
    }

}
