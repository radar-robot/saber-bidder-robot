package com.jxtech.saber.service.common;

import com.jxtech.saber.properties.ProjectConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by cook on 2019/9/14
 */
@Component
public class CommandExecPool implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(CommandExecPool.class);

    private static ExecutorService pool;

    private static ExecutorService heavyPool;

    @Autowired
    private ProjectConfig conf;

    public void execute(Runnable runnable) {
        pool.execute(runnable);
    }

    public void executeHeavy(Runnable runnable) {
        heavyPool.execute(runnable);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        pool = Executors.newFixedThreadPool(conf.getCommandAsyncPoolSize());
        heavyPool = Executors.newFixedThreadPool(conf.getHeavyAsyncPoolSize());

        logger.info("init command pool with size#{} and heavy-size#{}", conf.getCommandAsyncPoolSize(), conf.getHeavyAsyncPoolSize());
    }

}
