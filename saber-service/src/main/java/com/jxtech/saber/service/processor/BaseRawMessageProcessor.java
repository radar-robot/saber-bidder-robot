package com.jxtech.saber.service.processor;

import org.springframework.beans.factory.InitializingBean;

/**
 * Created by cook on 2019/3/30
 */
public abstract class BaseRawMessageProcessor implements RawMessageProcessor, InitializingBean {


    @Override
    public void afterPropertiesSet() throws Exception {
        RawMessageFactory.register(this);
    }

}
