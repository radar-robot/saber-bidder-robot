package com.jxtech.saber.service.bizs.sys;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.jxtech.saber.properties.ProjectConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 码工管理器
 * Created by cook on 2019/3/25
 */
@Component
public class AccountUserManager {

    private static final Logger logger = LoggerFactory.getLogger(AccountUserManager.class);

    private static final Cache<String, String> accountPrinciples = CacheBuilder.newBuilder()
            .maximumSize(100 * 1000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .build();

    @Autowired
    private ProjectConfig conf;

    /**
     *
     * @param accountNo
     * @param principle
     */
    public void setAccountPrinciple(String accountNo, String principle) {
        if (StringUtils.isEmpty(accountNo)) {
            return;
        }

        accountPrinciples.put(accountNo, principle);
    }

    public String getAccountPrinciple(String accountNo) {
        if (StringUtils.isEmpty(accountNo)) {
            return null;
        }
        return accountPrinciples.getIfPresent(accountNo);
    }

}
