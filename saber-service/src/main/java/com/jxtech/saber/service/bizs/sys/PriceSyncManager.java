package com.jxtech.saber.service.bizs.sys;

import com.fxtech.panda.core.utils.LocalDateUtils;
import com.google.common.collect.Interner;
import com.google.common.collect.Interners;
import com.google.common.collect.Maps;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.model.dto.sys.PriceActionRequest;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.properties.ProjectConfig;
import com.jxtech.saber.service.common.CommandExecPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by cook on 2019/9/8
 */
@Component
public class PriceSyncManager implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(PriceSyncManager.class);

    private static final Interner<String> lock = Interners.newWeakInterner();

    @Autowired
    private CommandExecPool execPool;

    @Autowired
    private RadarClientManager userManager;

    @Autowired
    private BidderCommandService commandService;

    @Autowired
    private ProjectConfig conf;

    private Map<String, ScreenPriceState> bidderPriceMap;

    private int[] flags;

    public void reInit() {
        bidderPriceMap.forEach((k, v) -> {
            v.reInit();
        });
        for (int i = 0; i < 60; i++) {
            flags[i] = 0;
        }
        logger.info("re-init sync-manager");
    }

    /**
     * 如果是第一次出现 该秒的价格, 会返回true, 用于标示 可以去通知其他slow客户端当前 该秒 价格
     * @param req
     * @return
     */
    public boolean infoPrice(PriceActionRequest req) {
        ScreenPriceState state = bidderPriceMap.computeIfAbsent(req.getMachineCode(), k -> new ScreenPriceState());
        state.add(req.getScreenTime(), req.getScreenPrice());

        int sec = req.getScreenTime().getSecond();
        if (flags[sec] == 1) {
            return false;
        }

        synchronized (lock.intern(String.valueOf(sec))) {
            if (flags[sec] == 0) {
                flags[sec] = 1;
                return true;
            }
        }

        return false;
    }

    public void trySyncPriceToSlowBidder(LocalDateTime screenTime, int basePrice) {
        long s1 = System.currentTimeMillis();
        int sec = screenTime.getSecond();
        String text = LocalDateUtils.toMills(screenTime) + "," + basePrice;
        List<BidderClient> bidders = userManager.queryWorkingClients();
        int slowCount = 0;
        for (BidderClient bidder : bidders) {
            ScreenPriceState state = bidderPriceMap.get(bidder.id());
            if (state == null || state.getPrice(sec) == null) {
                slowCount++;
                execPool.execute(() -> {
                    commandService.sendCommand(bidder, CommandDirective.PRICE_TELL, text);
                });
            }
        }

        if (slowCount > 0) {
            logger.info("sync price to slow-bidders#{}, elapsed {}ms", slowCount, System.currentTimeMillis() - s1);
        }
    }

    public void addBidderPriceState(String bidderId) {
        bidderPriceMap.put(bidderId, new ScreenPriceState());
    }

    @Override
    public void afterPropertiesSet() {
        this.bidderPriceMap = Maps.newConcurrentMap();
        this.flags = new int[60];
        reInit();
    }
}
