package com.jxtech.saber.service.bizs.sys;

import com.jxtech.saber.model.bo.PagePrice;

import java.time.LocalDateTime;

/**
 * 价格 中心聚集
 * Created by cook on 2019/9/14
 */
public class ScreenPriceState {

    private PagePrice[] prices;

    public ScreenPriceState() {
        prices = new PagePrice[60];
        this.reInit();
    }

    public void reInit() {
        for (int i = 0; i < 60; i++) {
            prices[i] = null;
        }
    }

    public void add(LocalDateTime dt, int price) {
        prices[dt.getSecond()] = new PagePrice(dt, price);
    }

    public PagePrice getPrice(int sec) {
        return prices[sec];
    }

    public boolean isPriceReady(int sec) {
        return prices[sec] != null;
    }

}
