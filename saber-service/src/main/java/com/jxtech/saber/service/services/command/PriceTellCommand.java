package com.jxtech.saber.service.services.command;

import com.fxtech.panda.core.json.Jack;
import com.fxtech.panda.core.utils.LocalDateUtils;
import com.fxtech.panda.core.utils.ThreadUtils;
import com.google.common.collect.Lists;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.model.dto.sys.BidderLoginResponse;
import com.jxtech.saber.model.dto.sys.PriceActionRequest;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.model.enums.PriceAction;
import com.jxtech.saber.service.bizs.sys.PriceSyncManager;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.service.common.CommandExecPool;
import com.jxtech.saber.support.command.BaseCommand;
import com.jxtech.saber.support.message.JsonCommand;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Iterator;

/**
 * 价格上班 并发会很高, 所以全部异步处理
 * Created by cook on 2019/5/26
 */
@Component
public class PriceTellCommand extends BaseCommand<String> implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(PriceTellCommand.class);

    @Autowired
    private CommandExecPool execPool;

    @Autowired
    private RadarClientManager userManager;

    @Autowired
    private PriceSyncManager priceSyncManager;

    @Override
    public JsonCommand doExecute(ChannelHandlerContext ctx, int clintNo, String reqText) {
        PriceActionRequest req = PriceActionRequest.fromLine(reqText);

        if (req.getAction() == PriceAction.PRICE_SHOW) {
            handlePriceShow(req);
            return null;
        }

        execPool.execute(() -> {
            handlePriceSubmitOrOffer(req);
        });
        return null;
    }

    private void handlePriceSubmitOrOffer(PriceActionRequest req) {
        logger.info("price submit/offer acton is {}", Jack.toJson(req));

        BidderClient bidder = userManager.getByMachineCode(req.getMachineCode());
        LocalDateTime screenTime = req.getScreenTime();
        int minute = screenTime.getMinute();
        int currentSec = screenTime.getSecond();

        if (bidder.getPhase2Actions() == null) {
            bidder.setPhase2Actions(Lists.newArrayList());
        }

//        bidder.getPhase2Actions().removeIf(r -> r.getScreenTime().getMinute() >= minute
//                & r.getScreenTime().getSecond() > currentSec);

        bidder.getPhase2Actions().add(req);

        // 保留最新的9条记录
        if (bidder.getPhase2Actions().size() > 8) {
            // 一次移除3个
            Iterator<PriceActionRequest> iter = bidder.getPhase2Actions().iterator();
            int i = 0;
            while (iter.hasNext()) {
                if (i < 3) {
                    iter.remove();
                } else {
                    break;
                }

                i++;
            }

        }

        BidderLoginResponse resp = new BidderLoginResponse();
        resp.setClientNo(bidder.getAssignedClientNo());
    }

    private void handlePriceShow(PriceActionRequest req) {
        logger.info("price show: {}", toText(req));

        boolean canSync = priceSyncManager.infoPrice(req);
        if (!canSync) {
            return;
        }

        // 最多只同步25秒之后的价格
        if (req.getScreenTime().getSecond() > 25) {
            execPool.executeHeavy(() -> {
                // 这里考虑 延迟15ms执行, 为了等待其他客户端检测价格
                ThreadUtils.sleep(15);
                priceSyncManager.trySyncPriceToSlowBidder(req.getScreenTime(), req.getScreenPrice());
            });
        } else {
            // 25秒之前的价格, 就用于init
            priceSyncManager.reInit();
        }
    }

    private String toText(PriceActionRequest req) {
        return req.getMachineCode() + " " + req.getScreenTime().getMinute() + ":" + req.getScreenTime().getSecond() + " " + req.getScreenPrice() ;
    }

    @Override
    public CommandDirective getDirective() {
        return CommandDirective.PRICE_TELL;
    }

}
