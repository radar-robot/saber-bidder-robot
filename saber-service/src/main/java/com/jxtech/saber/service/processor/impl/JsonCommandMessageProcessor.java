package com.jxtech.saber.service.processor.impl;

import com.fxtech.panda.core.json.Jack;
import com.jxtech.saber.service.processor.BaseRawMessageChannelProcessor;
import com.jxtech.saber.support.command.CommandProcessorFactory;
import com.jxtech.saber.support.command.ICommand;
import com.jxtech.saber.support.common.RawMessageType;
import com.jxtech.saber.support.message.JsonCommand;
import com.jxtech.saber.support.message.RawMessage;
import com.jxtech.saber.support.utils.RawMessages;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/3/31
 */
@Component
public class JsonCommandMessageProcessor extends BaseRawMessageChannelProcessor {

    private static final Logger logger = LoggerFactory.getLogger(JsonCommandMessageProcessor.class);

    @Override
    public RawMessage process(ChannelHandlerContext ctx, RawMessage msg) {
        JsonCommand commandReq = RawMessages.parseJsonCommand(msg.getClientNo(), msg.getBodyText());

        ICommand command = CommandProcessorFactory.getCommand(commandReq.getDirective());

        JsonCommand commandResp = command.execute(ctx, commandReq);
        if (commandResp == null) {// no need to response
            return null;
        }

        commandResp.setClientNo(msg.getClientNo());
        commandResp.setRequestId(commandReq.getRequestId());

        return RawMessages.buildJsonMessage(msg.getClientNo(), Jack.toJson(commandResp));
    }

    @Override
    public RawMessageType messageType() {
        return RawMessageType.JSON_COMMAND;
    }

}
