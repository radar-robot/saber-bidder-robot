package com.jxtech.saber.service.socket.netty;

import com.jxtech.saber.service.socket.netty.handler.RawMessageInboundHandler;
import com.jxtech.saber.service.socket.netty.handler.RawMessageOutboundHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/3/30
 */
@Component
public class SaberServerInitializer extends ChannelInitializer<SocketChannel> {

    @Autowired
    private RawMessageOutboundHandler rawMessageOutboundHandler;

    @Autowired
    private RawMessageInboundHandler rawMessageInboundHandler;

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast(rawMessageOutboundHandler);

        pipeline.addLast(rawMessageInboundHandler);

    }
}
