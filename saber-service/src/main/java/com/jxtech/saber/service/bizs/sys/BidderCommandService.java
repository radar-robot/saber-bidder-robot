package com.jxtech.saber.service.bizs.sys;

import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.json.Jack;
import com.fxtech.panda.core.utils.DataResults;
import com.jxtech.saber.model.dto.ICommandRequest;
import com.jxtech.saber.model.dto.sys.*;
import com.jxtech.saber.service.processor.RawMessageEncoder;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.support.message.JsonCommand;
import com.jxtech.saber.support.message.RawMessage;
import com.jxtech.saber.support.utils.B;
import com.jxtech.saber.support.utils.JsonCommands;
import com.jxtech.saber.support.utils.RawMessages;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cook on 2019/6/2
 */
@Component
public class BidderCommandService {

    @Autowired
    private RawMessageEncoder messageEncoder;

    @Autowired
    private RadarClientManager radarClientManager;

    @Autowired
    private BidderChannelManager bidderChannelManager;

    /**
     *
     * @param clientNo
     * @return
     */
    public DataResult<String> captureScreen(int clientNo) {
        BidderCaptureScreenRequest req = new BidderCaptureScreenRequest();

        DataResult<String> ret = sendCommand(clientNo, CommandDirective.CAPTURE_BID_SCREEN, req);

        return ret;
    }

    public DataResult<String> bidAccountLogin(BidAccountLoginRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> leftClick(LeftClickCommandRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> inputText(InputTextCommandRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> phase1PriceOffer(Phase1PriceOfferRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> phase1PriceSubmit(Phase1PriceSubmitRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> setBidStrategies(BidStrategiesSetRequest req) {
        DataResult<String> ret = sendCommand(req.getClientNo(), req.getDirective(), req);

        return ret;
    }

    public DataResult<String> leftClick(int clientNo) {
        BidderCaptureScreenRequest req = new BidderCaptureScreenRequest();

        DataResult<String> ret = sendCommand(clientNo, CommandDirective.LEFT_CLICK, req);

        return ret;
    }

    public DataResult<String> inputText(int clientNo) {
        BidderCaptureScreenRequest req = new BidderCaptureScreenRequest();

        DataResult<String> ret = sendCommand(clientNo, CommandDirective.LEFT_CLICK, req);

        return ret;
    }


    public DataResult<String> sendCommand(int clientNo, CommandDirective directive, ICommandRequest commReq) {

        return sendCommand(clientNo, directive, Jack.toJson(commReq));
    }

    public DataResult<String> sendCommand(int clientNo, CommandDirective directive, String commandData) {
        if (clientNo <= 0) {
            return DataResults.fail(-1, "clientNo的账号非法：" + clientNo);
        }

        BidderClient client = radarClientManager.getByClientNo(clientNo);
        if (client == null) {
            return DataResults.fail(-1, "clientNo的账号无：" + clientNo);
        }

        return sendCommand(client, directive, commandData);
    }

    public DataResult<String> sendCommand(BidderClient client, CommandDirective directive, String commandData) {
        JsonCommand commandReq = JsonCommands.ok(directive, commandData);
        commandReq.setClientNo(client.getAssignedClientNo());

        RawMessage raw = RawMessages.buildJsonMessage(client.getAssignedClientNo(), Jack.toJson(commandReq));

        byte[] bytesData = messageEncoder.encode(raw);

        ChannelHandlerContext ctx = bidderChannelManager.getByBidderId(client.getMachineCode());
        ctx.writeAndFlush(B.wrapBytes(ctx, bytesData));

        return DataResults.ok("");
    }

    public DataResult<String> sendCommand(String bidderId, CommandDirective directive, String commandData) {
        if (StringUtils.isEmpty(bidderId)) {
            return DataResults.fail(-1, "bidderId的账号非法：" + bidderId);
        }

        BidderClient client = radarClientManager.getByMachineCode(bidderId);
        if (client == null) {
            return DataResults.fail(-1, "bidderId的账号无：" + bidderId);
        }

        return sendCommand(client, directive, commandData);
    }

    public DataResult<String> batchSendCommand(List<Integer> clientNos, CommandDirective directive, String commandData) {
        if (CollectionUtils.isEmpty(clientNos)) {
            return DataResults.ok("no client-nos");
        }

        clientNos.parallelStream()
                .forEach(no -> {
                    DataResult<String> ret = sendCommand(Integer.valueOf(no), directive, commandData);
                });

        return DataResults.ok("");
    }

}
