package com.jxtech.saber.service.bizs.sys;

import com.google.common.collect.Maps;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.service.socket.netty.BidderChannelContext;
import com.jxtech.saber.support.utils.K;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by cook on 2019/3/31
 */
@Component
public class BidderChannelManager {

    // channelId -- context
    private static final Map<String, BidderChannelContext> channelIdMaps = Maps.newConcurrentMap();

    // bidderId -- context
    private static final Map<String, BidderChannelContext> bidderIdMaps = Maps.newConcurrentMap();

    public void connect(ChannelHandlerContext ctx) {
        BidderChannelContext bcc = new BidderChannelContext(ctx);
        bcc.setConnectTime(LocalDateTime.now());

        channelIdMaps.put(bcc.getChannelId(), bcc);
    }

    public LocalDateTime getLastLoginTimeOrNow(String channelId) {
        return channelIdMaps.containsKey(channelId) ? channelIdMaps.get(channelId).getConnectTime() : null;
    }

    public ChannelHandlerContext getByBidderId(String bidderId) {
        return bidderIdMaps.containsKey(bidderId) ? bidderIdMaps.get(bidderId).getContext() : null;
    }

    public void register(String bidderId, ChannelHandlerContext ctx) {
        BidderChannelContext bcc = channelIdMaps.computeIfAbsent(K.getChannelId(ctx), k -> new BidderChannelContext(ctx));
        bcc.setBidderId(bidderId);

        bidderIdMaps.put(bidderId, bcc);
        channelIdMaps.put(bcc.getChannelId(), bcc);
    }

    public String getBidderId(ChannelHandlerContext ctx) {
        BidderChannelContext context = channelIdMaps.get(K.getChannelId(ctx));

        return context != null ? context.getBidderId() : null;
    }

    public void disconnect(ChannelHandlerContext ctx, boolean isTransportError) {
        String cId = K.getChannelId(ctx);

        channelIdMaps.remove(cId);
    }

}
