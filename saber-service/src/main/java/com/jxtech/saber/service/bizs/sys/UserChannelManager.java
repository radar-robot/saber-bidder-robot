package com.jxtech.saber.service.bizs.sys;

import com.google.common.collect.Lists;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.service.processor.RawMessageEncoder;
import com.jxtech.saber.support.message.RawMessage;
import com.jxtech.saber.support.utils.B;
import com.jxtech.saber.support.utils.RawMessages;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cook on 2019/3/31
 */
@Component
public class UserChannelManager {

    private static final Logger logger = LoggerFactory.getLogger(UserChannelManager.class);

    @Autowired
    private RadarClientManager userManager;

    @Autowired
    private BidderChannelManager channelManager;

    @Autowired
    private RawMessageEncoder messageEncoder;

    public int sendCommand(String command) {
        List<ChannelHandlerContext> ctxs = queryWorkingChannels();

        RawMessage msg = RawMessages.buildJsonMessage(556677, command);


        for (ChannelHandlerContext ctx : ctxs) {
            ctx.writeAndFlush(B.wrapBytes(ctx, messageEncoder.encode(msg)));
        }

        return ctxs.size();
    }

    /**
     *
     * @return
     */
    public List<ChannelHandlerContext> queryWorkingChannels() {
        List<BidderClient> bidders = userManager.queryWorkingClients();
        List<ChannelHandlerContext> ret = Lists.newArrayList();

        for (BidderClient bidder : bidders) {
            ChannelHandlerContext ctx = channelManager.getByBidderId(bidder.id());
            ret.add(ctx);
        }

        return ret;
    }

    public void disconnect(ChannelHandlerContext ctx, boolean isTransportError) {
        String bidderId = channelManager.getBidderId(ctx);
        if (StringUtils.isBlank(bidderId)) {
            logger.info("ctx#{} already disconnected, isTransportError is {}", ctx, isTransportError);
            return;
        }
        userManager.disconnect(bidderId, isTransportError);

        channelManager.disconnect(ctx, isTransportError);
    }

}
