package com.jxtech.saber.service.services.sys;

import com.jxtech.saber.model.dto.sys.TestJustResponse;
import com.jxtech.saber.properties.ProjectConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cook on 2019/1/25
 */
@Service
public class TestJustService {


    @Autowired
    private ProjectConfig config;

    public TestJustResponse getOneJust() {
        // TODO:
        TestJustResponse test1 = new TestJustResponse();
        test1.setId(config.getJustValue());
        test1.setName("大大的测试-" + config.getJustValue());
        test1.setPath("/test/just");
        test1.setIcon("my-icon");

        return test1;
    }

}
