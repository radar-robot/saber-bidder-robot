package com.jxtech.saber.service.processor;

import com.jxtech.saber.support.message.RawMessage;
import com.jxtech.saber.support.utils.B;
import org.springframework.stereotype.Component;

/**
 * Created by cook on 2019/3/30
 */
@Component
public class RawMessageEncoder {

    public RawMessage decode(byte[] data) {
        RawMessage msg = new RawMessage();
        msg.setTotalLength(B.readInt(data, 0));
        msg.setMagic(B.readInt(data, 4));
        msg.setOccurMills(B.readLong(data, 8));

        msg.setClientNo(B.readInt(data, 16));
        msg.setMessageType(B.readInt(data, 20));
        msg.setBodyLength(B.readInt(data, 24));

        msg.setBodyText(new String(B.readFixLength(data, 28, msg.getBodyLength())));

        return msg;
    }

    public byte[] encode(RawMessage msg) {
        byte[] data = new byte[msg.getTotalLength()];

        B.writeInt(data, 0, msg.getTotalLength());
        B.writeInt(data, 4, msg.getMagic());
        B.writeLong(data, 8, msg.getOccurMills());

        B.writeInt(data, 16, msg.getClientNo());
        B.writeInt(data, 20, msg.getMessageType());
        B.writeInt(data, 24, msg.getBodyLength());

        B.writeBytes(data, 28, msg.getBodyText().getBytes());

        return data;
    }

}
