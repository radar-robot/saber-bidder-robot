package com.jxtech.saber.service.services.command;

import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.model.dto.sys.BidderRegisterRequest;
import com.jxtech.saber.model.dto.sys.BidderLoginResponse;
import com.jxtech.saber.service.bizs.sys.BidderChannelManager;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.support.command.BaseCommand;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.support.message.JsonCommand;
import com.jxtech.saber.support.utils.JsonCommands;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by cook on 2019/5/26
 */
@Component
public class ClientRegisterCommand extends BaseCommand<BidderRegisterRequest> {

    @Autowired
    private BidderChannelManager channelManager;

    @Autowired
    private RadarClientManager userManager;

    @Override
    public JsonCommand doExecute(ChannelHandlerContext ctx, int clintNo, BidderRegisterRequest req) {
        BidderClient bidder = userManager.register(req);
        bidder.setClientVersion(req.getClientVersion());
        bidder.getLoginCount().incrementAndGet();
        bidder.setMachineAddress(req.getLocalIpAddress());
        bidder.setLastLoginAddress(ctx.channel().remoteAddress().toString());
        bidder.setLastConnectedTime(channelManager.getLastLoginTimeOrNow(ctx.channel().id().asLongText()));
        bidder.setSeatNo(req.getSeatNo());

        channelManager.register(bidder.id(), ctx);

        BidderLoginResponse resp = new BidderLoginResponse();
        resp.setClientNo(bidder.getAssignedClientNo());
        resp.setServerTime(LocalDateTime.now());

        return JsonCommands.ok(CommandDirective.RESP_REGISTER_LOGIN, resp);
    }

    @Override
    public CommandDirective getDirective() {
        return CommandDirective.CLIENT_REGISTER;
    }

}
