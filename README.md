## 项目


#test
## 启动
* 运行 *com.jxtech.saber.api.main.ApplicationStart*

## 自动生成
### how
*  配置 saber-dao/src/test/resources/dev_jdbc.properties
*  运行 com.jxtech.saber.dao.test.generate.MainBeanGenerator

### 开关
1. 生成DTORequest, ***forceOverrideDtoRequest***开关，是否强制覆盖DtoRequest文件, 默认: false
2. 生成DTOResponse, ***forceOverrideDtoRequest***开关，是否强制覆盖DtoResponse文件, 默认: false
3. 生成Bean, ***forceOverrideDtoRequest***开关，是否强制覆盖Bean文件, 默认: true
4. 生成Dao, ***forceOverrideDtoRequest***开关，是否强制覆盖Dao文件, 默认: false
5. 生成Service, ***forceOverrideDtoRequest***开关，是否强制覆盖Service文件, 默认: false
6. 生成IService, 在facade下
7. 生成BaseMapper.xml, ***forceOverrideDtoRequest***开关，是否强制覆盖BaseMapper文件, 默认: true
8. 生成Controller, ***enableController***开关，是否生成Controller文件, 默认: false
9. removePrefixes 去除表的前缀: 例如 cmn_, sln_, mkt_

### 关于dao和mapper
* 所有的dao继承 *com.hujiang.paladin.logging.dao.common.CrudDao*
* mapper.xml有两个文件，
    1. 一个BaseMapper.xml里面是和*CrudDao*里的是一一对应的
    2. 一个Mapper.xml，内部写自定义的sql
    
 ### Redis命名规范
 * support/common 下增加一个 RedisPrefixKeys 常量定义
