package com.jxtech.saber.properties;

import com.google.common.base.Splitter;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by cook on 2018/9/26
 */
@Component
@Getter
public class ProjectConfig {

    @Value("${conf.test.prop:0}")
    private int justValue;

    @Value("${saber.socket.server.port:9966}")
    private int socketServerPort;

    @Value("${saber.client.update.location}")
    private String clientUpdateLocation;

    @Value("${saber.admin.accounts:saber1,zdf!34Z;saber2,zdf!a7Z;saber3,xdfg434#G;saber4,xGfg434#G}")
    private String adminAccounts;


    @Value("${screen.images.dir}")
    private String screenImagesDir;

    @Value("${screen.images.resource.path}")
    private String screenImagesResourcePath;

    @Value("${screen.image.url-prefix}")
    private String screenImageUrlPrefix;

    @Value("${screen.image.address-prefix:}")
    private String screenImageAddressPrefix;

    @Value("${command.async.pool-size:2}")
    private int commandAsyncPoolSize;

    @Value("${heavy.async.pool-size:2}")
    private int heavyAsyncPoolSize;



    /**
     *
     * @return
     */
    public Map<String, String> getAdminAccounts() {
        return Splitter.on(";").withKeyValueSeparator(',').split(adminAccounts);
    }


}
