package com.jxtech.saber.support.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jxtech.saber.support.common.RequestSequence;
import com.jxtech.saber.model.enums.CommandDirective;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/5/26
 */
@Getter
@Setter
public class JsonCommand {

    //
    private long requestId;

    private int clientNo;

    //
    private int directiveVal;

    private String data;

    //
    private int status;

    private String message;

    public JsonCommand() {

    }

    public JsonCommand(CommandDirective directive, int status, String data, String message) {
        setDirective(directive);
        this.status = status;
        this.data = data;
        this.message = message;
        this.requestId = RequestSequence.next();
    }

    @JsonIgnore
    public CommandDirective getDirective() {
        return CommandDirective.valueOf(directiveVal);
    }

    @JsonIgnore
    public void setDirective(CommandDirective directive) {
        this.directiveVal = directive.getCode();
    }

}
