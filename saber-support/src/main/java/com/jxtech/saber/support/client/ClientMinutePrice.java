package com.jxtech.saber.support.client;

import com.jxtech.saber.support.utils.K;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cook on 2019/6/15
 */
@Getter
@Setter
public class ClientMinutePrice {

    private static final Logger logger = LoggerFactory.getLogger(ClientMinutePrice.class);

    private int[] prices;

    public ClientMinutePrice() {
        prices = new int[60];
        for (int i=0; i<60; i++) {
            prices[i] = 0;
        }
    }

    public void addSecPrice(int sec, int price) {
        if (K.isNotSecond(sec)) {
            logger.error("illegal second#{}", sec);
            return;
        }

        // 已经存在, 则不需要
        if (prices[sec] > 0) {

            return;
        }

        prices[sec] = price;
    }

    public int getSecPrice(int sec) {
        if (K.isNotSecond(sec)) {
            logger.error("illegal second#{}", sec);
            return -1;
        }
        return prices[sec];
    }

    public void clear() {
        for (int i=0; i<60; i++) {
            prices[i] = 0;
        }
    }

}
