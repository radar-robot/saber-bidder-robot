package com.jxtech.saber.support.utils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by cook on 2019/3/5
 */
public class B {

    /**
     *
     */
    public static final int MAGIC_NUMBER = 2140483647;

    // write

    public static void writeLong(byte[] data, int offset, long value) {
        data[offset] = (byte) ((value >> 56) & 0xFF);
        data[offset+1] = (byte) ((value >> 48) & 0xFF);
        data[offset+2] = (byte) ((value >> 40) & 0xFF);
        data[offset+3] = (byte) ((value >> 32) & 0xFF);
        data[offset+4] = (byte) ((value >> 24) & 0xFF);
        data[offset+5] = (byte) ((value >> 16) & 0xFF);
        data[offset+6] = (byte) ((value >> 8) & 0xFF);
        data[offset+7] = (byte) ((value) & 0xFF);
    }

    public static void writeInt(byte[] data, int offset, int value) {
        data[offset] = (byte) ((value >> 24) & 0xFF);
        data[offset+1] = (byte) ((value >> 16) & 0xFF);
        data[offset+2] = (byte) ((value >> 8) & 0xFF);
        data[offset+3] = (byte) ((value) & 0xFF);
    }

    public static void writeByte(byte[] data, int offset, byte value) {
        data[offset] = value;
    }

    public static void writeBytes(byte[] data, int offset, byte[] bs) {
        for (int i=0; i<bs.length; i++) {
            data[offset+i] = bs[i];
        }
    }

    // read

    public static byte read1Byte(byte[] bs, int offset) {
        return bs[offset];
    }

    public static byte[] read4Bytes(byte[] bs, int offset) {
        return new byte[]{bs[offset], bs[offset + 1], bs[offset + 2], bs[offset + 3]};
    }

    public static byte[] readFixLength(byte[] bs, int offset, int len) {
        byte[] tar = new byte[len];
        System.arraycopy(bs, offset, tar, 0, len);
        return tar;
    }

    /**
     * f1
     * @param bs
     * @param offset
     * @return
     */
    public static int readInt(byte[] bs, int offset) {
        byte[] seg = read4Bytes(bs, offset);
        int ret = ((int) seg[0] & 0xFF) << 24 |
                ((int) seg[1] & 0xFF) << 16 |
                ((int) seg[2] & 0xFF) << 8 |
                ((int) seg[3] & 0xFF);
        return ret;
    }

    /**
     * f1
     * @param bs
     * @param offset
     * @return
     */
    public static long readLong(byte[] bs, int offset) {
        byte[] seg = readFixLength(bs, offset, 8);

        long ret = ((long) seg[0] & 0xFF) << 56 |
                ((long) seg[1] & 0xFF) << 48 |
                ((long) seg[2] & 0xFF) << 40 |
                ((long) seg[3] & 0xFF) << 32 |
                ((long) seg[4] & 0xFF) << 24 |
                ((long) seg[5] & 0xFF) << 16 |
                ((long) seg[6] & 0xFF) << 8 |
                ((long) seg[7] & 0xFF)
                ;
        return ret;
    }

    //

    /**
     *
     * @param bytes
     * @return
     */
    public static String formatPrettyBytes(byte[] bytes) {
        int group = bytes.length / 16;
        group = bytes.length % 16 == 0 ? group : group + 1;
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        for (int i = 0; i < group; i++) {
            for (int j=0; j<16; j++) {
                int offset = i * 16 + j;
                if (offset >= bytes.length) {
                    break;
                }
                try {
                    sb.append(String.format("%02x ", bytes[offset] & 0xff));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static ByteBuf wrapBytes(ChannelHandlerContext ctx, byte[] data) {
        ByteBuf buf = ctx.alloc().heapBuffer();
        buf.writeBytes(data);
        return buf;
    }

}
