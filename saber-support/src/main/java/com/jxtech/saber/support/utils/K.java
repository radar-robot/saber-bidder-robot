package com.jxtech.saber.support.utils;

import com.jxtech.saber.support.common.FrontPage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * Created by cook on 2019/3/31
 */
public class K {

    private static final Logger logger = LoggerFactory.getLogger(K.class);

    public static String getChannelId(ChannelHandlerContext ctx) {
        return ctx.channel().id().asLongText();
    }


    public static FrontPage convert(HttpServletRequest request) {
        FrontPage page = new FrontPage();
        page.setSearch(request.getParameter("search"));
        String limit = request.getParameter("limit");
        String offset = request.getParameter("offset");
        page.setLimit(StringUtils.isNotBlank(limit) ? Integer.valueOf(limit) : 20);
        page.setOffset(StringUtils.isNotBlank(offset) ? Integer.valueOf(offset) : 0);
        String order = request.getParameter("order");
        page.setOrder(StringUtils.isNotBlank(order) ? order : "asc");
        page.setSort(request.getParameter("sort"));
        return page;
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static boolean isSecond(int sec) {
        if (sec < 0 || sec > 59) {
            return false;
        }

        return true;
    }

    public static boolean isNotSecond(int sec) {
        if (sec > 0 && sec < 59) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param buf
     * @return
     */
    public static boolean release(ByteBuf buf) {
        try {
            return ReferenceCountUtil.release(buf);
        } catch (Exception e) {
            logger.error("release buf error", e);
        }

        return false;
    }

}
