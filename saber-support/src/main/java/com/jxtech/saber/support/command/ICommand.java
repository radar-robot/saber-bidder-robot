package com.jxtech.saber.support.command;

import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.support.message.JsonCommand;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by cook on 2019/5/26
 */
public interface ICommand {

    /**
     *
     * @param ctx
     * @param comm
     * @return
     */
    JsonCommand execute(ChannelHandlerContext ctx, JsonCommand comm);

    /**
     *
     * @return
     */
    CommandDirective getDirective();

}
