package com.jxtech.saber.support.json;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fxtech.panda.core.enums.EnumHelper;

import java.io.IOException;

/**
 * Created by cook on 2017/12/29
 */
public class JsonEnumDeserializer<T extends Enum> extends JsonDeserializer<T> {

    private Class<T> enumType;

    public JsonEnumDeserializer(Class<T> enumType) {
        this.enumType = enumType;
    }

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String source = p.getText();
        return EnumHelper.textToEnum(source, enumType);
    }

}
