package com.jxtech.saber.support.common;

import com.fxtech.panda.core.enums.CodeDescriptionFeature;
import com.fxtech.panda.core.enums.EnumHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by cook on 2019/3/30
 */
@Getter
@AllArgsConstructor
public enum RawMessageType implements CodeDescriptionFeature {

    JSON_COMMAND(10002, "消息Data格式为JSON, 且为命令模式"),

    PING_COMMAND(10101, "消息Data格式为文本, 且为ping模式"),

    ;

    private int code;

    private String description;

    public static RawMessageType valueOf(Integer code) {
        return EnumHelper.valueOf(code, RawMessageType.class);
    }

}
