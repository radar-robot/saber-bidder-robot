package com.jxtech.saber.support.command;

import com.jxtech.saber.model.enums.CommandDirective;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cook on 2019/5/26
 */
public class CommandProcessorFactory {

    private static final Map<CommandDirective, BaseCommand> commands = new HashMap<>();

    public static void register(BaseCommand<?> command) {
        commands.put(command.getDirective(), command);
    }

    public static ICommand getCommand(CommandDirective commandName) {
        return commands.get(commandName);
    }

}
