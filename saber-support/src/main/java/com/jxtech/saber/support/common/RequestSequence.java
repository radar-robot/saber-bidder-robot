package com.jxtech.saber.support.common;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by cook on 2019/6/2
 */
public class RequestSequence {

    private static final AtomicLong seq = new AtomicLong(11200000);

    public static long next() {
        return seq.incrementAndGet();
    }

}
