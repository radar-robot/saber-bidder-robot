package com.jxtech.saber.support.common;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by cook on 2017/8/13.
 */
@Getter
@Setter
public class FrontPageResult {
    private List<?> rows;
    private int total;

}
