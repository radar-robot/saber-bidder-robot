package com.jxtech.saber.support.common;


import com.fxtech.panda.core.utils.CamelCaseUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by cook on 2017/8/13.
 */
@Getter
@Setter
public class FrontPage {
    // [search: yyyy, order: asc, limit: 20, offset: 0]
    // [sort: jobStartTime, order: desc, limit: 20, offset: 0]
    private String search;
    private String sort;
    private String order = "asc";
    private int limit;
    private int offset;

    public String getOrderByField(String defaultField) {
        return StringUtils.isNotEmpty(sort) ? CamelCaseUtils.toHumpWord(sort) : defaultField;
    }

    public String getOrderByClause(String defaultField) {
        return getOrderByField(defaultField).replace("_text", "") + " " + order;
    }

    public String getSearchLike() {
        if (StringUtils.isBlank(search)) {
            return null;
        } else {
            return "%" + search.trim() + "%";
        }
    }

}
