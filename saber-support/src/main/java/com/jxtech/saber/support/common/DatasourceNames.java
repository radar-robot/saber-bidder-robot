package com.jxtech.saber.support.common;

/**
 * 数据源名称
 * Created by cook on 2019/1/5
 */
public interface DatasourceNames {

    /**
     * 主库
     */
    String NAME_MASTER = "master";

    /**
     * 从库
     */
    String NAME_SLAVE = "salve";

}
