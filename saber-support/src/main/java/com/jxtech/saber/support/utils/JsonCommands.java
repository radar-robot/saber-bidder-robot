package com.jxtech.saber.support.utils;

import com.fxtech.panda.core.utils.TextUtils;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.support.message.JsonCommand;

/**
 * Created by cook on 2019/6/1
 */
public class JsonCommands {


    /**
     * 判断 <code>JsonCommand</code> 是否成功, null表示失败
     * @param dr
     * @return
     */
    public static boolean isOk(JsonCommand dr) {
        return dr != null && dr.getStatus() == 0;
    }

    /**
     * 判断 <code>JsonCommand</code> 是否失败, null表示失败
     * @param dr
     * @return
     */
    public static boolean isFail(JsonCommand dr) {
        return !isOk(dr);
    }

    /**
     *
     * @param data
     * @return
     */
    public static JsonCommand ok(CommandDirective directive, Object data) {
        return new JsonCommand(directive, 0, TextUtils.valueToText(data), "");
    }

    /**
     *
     * @param message
     * @return
     */
    public static JsonCommand fail(CommandDirective directive, String message) {
        return new JsonCommand(directive, -1, null, message);
    }

    /**
     *
     * @param status
     * @param message
     * @return
     */
    public static JsonCommand fail(CommandDirective directive, int status, String message) {
        return new JsonCommand(directive, status, null, message);
    }

}
