package com.jxtech.saber.support.client;

import com.google.common.collect.Maps;
import com.jxtech.saber.support.utils.K;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by cook on 2019/6/15
 */

public class ClientMinuteState {

    private static final Logger logger = LoggerFactory.getLogger(ClientMinuteState.class);

    private int hour;

    private int minute;

    /**
     * 客户端秒数价格
     */
    private Map<Integer, ClientMinutePrice> mps;

    public ClientMinuteState(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        this.mps = Maps.newConcurrentMap();
    }

    /**
     * 重新放置一个客户端价格
     * @param clientNo
     */
    public void reputClient(int clientNo) {
        mps.put(clientNo, new ClientMinutePrice());
    }


    public void addClientSecPrice(int clientNo, int sec, int price) {
        if (K.isNotSecond(sec)) {
            logger.error("illegal second#{}", sec);
            return;
        }

        ClientMinutePrice mp = mps.get(clientNo);
        if (mp == null) {
            logger.warn("MP is null for client#{} sec#{} price#{}");
            reputClient(clientNo);
            mp = mps.get(clientNo);
        }

        mp.addSecPrice(sec, price);

    }

    public void clear() {

    }

}
