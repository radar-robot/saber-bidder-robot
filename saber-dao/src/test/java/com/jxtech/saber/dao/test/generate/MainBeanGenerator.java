package com.jxtech.saber.dao.test.generate;

import com.google.common.collect.Lists;
import org.lightning.particle.jdbc.ds.DataSourceParam;
import org.lightning.particle.jdbc.ds.DatasourceLoader;
import org.lightning.particle.plugin.javabean.common.GenerateOption;
import org.lightning.particle.plugin.javabean.meta.JavaFileGenerator;
import org.lightning.particle.plugin.javabean.meta.JdbcJavaFileGenerator;
import org.lightning.particle.plugin.javabean.utils.PathUtils;

/**
 * 自动生成java套件
 */
public class MainBeanGenerator {

    public static void main(String[] args) throws Exception {
        GenerateOption option = createGenerateOption();
        JavaFileGenerator fileGenerator = new JavaFileGenerator(rootDirectory);
        DataSourceParam param = DatasourceLoader.buildJdbcParam("local_dev");
        JdbcJavaFileGenerator generator = new JdbcJavaFileGenerator(param, option, fileGenerator);

        // 执行生成代码
        generator.generateJavaFileSuites();

        // 清除生成的代码 - 慎用!!
        //generator.cleanJavaFileSuitesCarefully();

        System.out.println("code generate done");
        System.exit(1);
    }

    private static GenerateOption createGenerateOption() {
        GenerateOption option = new GenerateOption();

        option.forceOverrideDtoRequest = true;
        option.forceOverrideDtoResponse = true;

        option.forceOverridePo = true;
        option.forceOverrideCriteria = true;
        option.forceOverrideBaseMapper = true;

        option.forceOverrideDao = false;
        option.forceOverrideService = false;
        option.forceOverrideBiz = false;
        option.forceOverrideController = false;
        option.enableController = true;
        option.enableLocalDateTime = true;

        // 移除的表名前缀
        option.removePrefixes = Lists.newArrayList("app_", "T_","t_");

        option.rootArtifactId = "saber";
        option.basePackageName = "com.jxtech.saber";
        return option;
    }

    private static final String jdbcResourcePath = "dev_jdbc.properties";
    private static final String currentArtifactId = "saber-dao";
    private static final String rootDirectory = PathUtils.getTopDir(MainBeanGenerator.class, currentArtifactId, jdbcResourcePath);

}
