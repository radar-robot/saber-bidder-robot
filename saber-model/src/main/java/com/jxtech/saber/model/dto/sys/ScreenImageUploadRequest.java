package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/6/30
 */
@Getter
@Setter
public class ScreenImageUploadRequest {

    private String machineCode;

    private String uid;

    private String from;

    private String token;

    private long timestamp;

    /**
     * 上传类型
     */
    private int uploadType;


}
