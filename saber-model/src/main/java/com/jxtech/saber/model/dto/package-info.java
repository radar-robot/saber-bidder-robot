/**
 * 放置DTO类型 - 需要被外部应用的业务实体 才需要放置在此处.
 */
package com.jxtech.saber.model.dto;