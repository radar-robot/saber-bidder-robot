package com.jxtech.saber.model.enums;

import com.fxtech.panda.core.enums.CodeDescriptionFeature;
import com.fxtech.panda.core.enums.EnumHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 控制指定 - 接收指令
 * Created by cook on 2019/5/19
 */
@Getter
@AllArgsConstructor
public enum CommandDirective implements CodeDescriptionFeature {

    NONE(0, "表示null"),

    CLIENT_REGISTER(60060, "客户端注册 - 请求"),
    RESP_REGISTER_LOGIN(60061, "客户端注册 - 响应"),


    SYNC_SYSTEM_TIME(90100, "同步NTP服务器时间"),
    CAPTURE_BID_SCREEN(90201, "截图flash屏幕"),
    UPLOAD_BID_SCREEN(90202, "上传flash屏幕"),
    CAPTURE_UPLOAD_BID_SCREEN(90203, "截图且上传flash屏幕"),
    CAPTURE_UPLOAD_FULL_SCREEN(90204, "截图且上传全屏幕"),

    UPLOAD_ZIPPED_LOG_FILE(90210, "上传压缩日志文件"),

    MOVE_CURSOR(90300, "移动鼠标"),
    LEFT_CLICK(90301, "左键点击"),
    INPUT_TEXT(90302, "输入文本"),

    DIRECTIVE_SCRIPT(90400, "指令脚本（多个指令的集合）"),


    PRICE_TELL(90500, "通知竞拍价格"),

    BID_ACCOUNT_LOGIN(90501, "竞拍账号登录"),

    PHASE1_PRICE_OFFER(90601, "第一阶段价格出价"),

    PHASE1_PRICE_SUBMIT(90602, "提交第一阶段价格提交"),

    BID_STRATEGIES_SET(90603, "设置竞拍策略"),

    ;

    private int code;

    private String description;

    public static CommandDirective valueOf(Integer code) {
        return EnumHelper.valueOf(code, CommandDirective.class);
    }


}
