package com.jxtech.saber.model.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * #机器码;竞拍账号;密码;身份证;竞拍策略
 * Created by cook on 2019/10/25
 */
@Getter
@Setter
public class AccountSetting {

    /**
     *
     */
    private String machineCode;

    /**
     *
     */
    private String accountNo;

    /**
     *
     */
    private String accountPswd;

    /**
     *
     */
    private String idCard;

    /**
     *
     */
    private String submitStrategy;

    public String toText() {
        StringBuilder sb = new StringBuilder();
        sb.append(machineCode).append(";")
                .append(accountNo).append(";")
                .append(accountPswd).append(";")
                .append(idCard).append(";")
                .append(submitStrategy).append(";");

        return sb.toString();
    }

}
