package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by cook on 2019/1/25
 */
@Getter
@Setter
public class TestJustResponse {

    /**
     * id
     */
    private int id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 访问地址
     */
    private String path;

    /**
     * 图标
     */
    private String icon;

    /**
     * 父级id
     */
    private int parentId;


}
