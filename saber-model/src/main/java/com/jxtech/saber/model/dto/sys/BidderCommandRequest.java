package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/3/31
 */
@Getter
@Setter
public class BidderCommandRequest {

    private String command;

}
