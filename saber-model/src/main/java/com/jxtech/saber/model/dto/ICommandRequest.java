package com.jxtech.saber.model.dto;

import com.jxtech.saber.model.enums.CommandDirective;

/**
 * Created by cook on 2019/6/2
 */
public interface ICommandRequest {

    CommandDirective getDirective();

    int getClientNo();


}
