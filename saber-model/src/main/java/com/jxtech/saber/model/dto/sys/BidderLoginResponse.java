package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by cook on 2019/3/31
 */
@Getter
@Setter
public class BidderLoginResponse {

    /**
     *
     */
    private int clientNo;

    /**
     *
     */
    private LocalDateTime serverTime;

}
