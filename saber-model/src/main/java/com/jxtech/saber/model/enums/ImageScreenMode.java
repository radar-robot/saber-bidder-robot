package com.jxtech.saber.model.enums;

import com.fxtech.panda.core.enums.CodeDescriptionFeature;
import com.fxtech.panda.core.enums.EnumHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by cook on 2019/8/11
 */
@Getter
@AllArgsConstructor
public enum ImageScreenMode implements CodeDescriptionFeature {

    FLASH_SCREEN(1, "Flash屏幕截图"),

    FULL_SCREEN(10, "全屏幕截图"),

            ;

    private int code;

    private String description;

    public static ImageScreenMode valueOf(Integer code) {
        return EnumHelper.valueOf(code, ImageScreenMode.class);
    }

}
