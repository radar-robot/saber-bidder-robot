package com.jxtech.saber.model.dto.sys;

import com.fxtech.panda.core.enums.EnumHelper;
import com.fxtech.panda.core.utils.LocalDateUtils;
import com.jxtech.saber.model.enums.PriceAction;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by cook on 2019/7/16
 */
@Getter
@Setter
public class PriceActionRequest {

    private String machineCode;

    private LocalDateTime occurTime;

    private LocalDateTime screenTime;

    private PriceAction action;

    /**
     * 当时的价格
     */
    private int screenPrice;

    /**
     * 目标价格
     */
    public int targetPrice;

    public int usedDelayMills;

    private String memo;

    public String getActionName() {
        return action != null ? action.getChName() : "-";
    }

    public String buildText() {
        return "";
    }

    public String toLine()
    {
        return machineCode + ";" + LocalDateUtils.toMills(occurTime) + ";" + LocalDateUtils.toMills(screenTime) + ";" + action + ";" + screenPrice + ";" + targetPrice + ";" + usedDelayMills + ";" + memo;
    }

    public static PriceActionRequest fromLine(String line)
    {
        String[] arr = line.split(";");
        PriceActionRequest req = new PriceActionRequest();
        req.machineCode = arr[0];
        req.occurTime = LocalDateUtils.toDateTime(Long.valueOf(arr[1]));
        req.screenTime = LocalDateUtils.toDateTime(Long.valueOf(arr[2]));
        req.action = PriceAction.valueOf(arr[3]);
        req.screenPrice = Integer.valueOf(arr[4]);
        req.targetPrice = Integer.valueOf(arr[5]);
        req.usedDelayMills = Integer.valueOf(arr[6]);
        req.memo = arr.length > 7 ? arr[7] : null;

        return req;
    }

}
