package com.jxtech.saber.model.dto.sys;

import com.jxtech.saber.model.dto.BaseCommandRequest;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/7/14
 */
@Getter
@Setter
public class BidStrategiesSetRequest extends BaseCommandRequest {

    // private String machineCode;

    private String bidStrategies;

}
