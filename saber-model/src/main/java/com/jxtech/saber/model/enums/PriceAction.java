package com.jxtech.saber.model.enums;

import com.fxtech.panda.core.enums.CodeDescriptionFeature;
import com.fxtech.panda.core.enums.EnumHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by cook on 2019/7/16
 */
@Getter
@AllArgsConstructor
public enum PriceAction implements CodeDescriptionFeature {

    PRICE_SHOW(1, "价格 - 显示", "H"),

    PRICE_OFFER(5, "价格 - 出价", "O"),

    PRICE_SUBMIT(6, "价格 - 提交", "S"),



    ;


    private int code;

    private String description;

    private String chName;

    public static PriceAction valueOf(Integer code) {
        return EnumHelper.valueOf(code, PriceAction.class);
    }

}
