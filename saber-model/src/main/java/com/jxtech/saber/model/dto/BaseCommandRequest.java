package com.jxtech.saber.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jxtech.saber.model.enums.CommandDirective;

/**
 * Created by cook on 2019/6/9
 */
public class BaseCommandRequest implements ICommandRequest {

    private int directiveVal;

    private int clientNo;

    private String machineCode;

    public int getDirectiveVal() {
        return directiveVal;
    }

    public void setDirectiveVal(int directiveVal) {
        this.directiveVal = directiveVal;
    }

    public int getClientNo() {
        return clientNo;
    }

    public void setClientNo(int clientNo) {
        this.clientNo = clientNo;
    }

    @JsonIgnore
    public CommandDirective getDirective() {
        return CommandDirective.valueOf(directiveVal);
    }

    @JsonIgnore
    public void setDirective(CommandDirective directive) {
        this.directiveVal = directive.getCode();
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }
}
