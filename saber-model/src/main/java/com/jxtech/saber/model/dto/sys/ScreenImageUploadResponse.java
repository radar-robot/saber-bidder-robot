package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by cook on 2019/6/30
 */
@Getter
@Setter
public class ScreenImageUploadResponse {

    private List<String> filePaths;

}
