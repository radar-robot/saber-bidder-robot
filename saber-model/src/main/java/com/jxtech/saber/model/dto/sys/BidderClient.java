package com.jxtech.saber.model.dto.sys;

import com.google.common.collect.Lists;
import com.jxtech.saber.model.enums.BidderStatus;
import com.jxtech.saber.model.enums.ImageScreenMode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by cook on 2019/3/31
 */
@Getter
@Setter
public class BidderClient {

    /**
     * 机器唯一地址
     */
    private String machineCode;

    /**
     * 机器IP
     */
    private String machineAddress;

    /**
     * 分配的客户端编号
     */
    private int assignedClientNo;

    /**
     * 状态
     */
    private BidderStatus status;

    /**
     * 客户端版本
     */
    private String clientVersion;

    /**
     * 最后一次存活Ping时间
     */
    private LocalDateTime lastLivePingTime;

    /**
     * 回答验证码次数
     */
    private AtomicInteger answerCount = new AtomicInteger();

    /**
     * 登录次数
     */
    private AtomicInteger loginCount = new AtomicInteger();

    /**
     * 最后登录地址 - IP
     */
    private String lastLoginAddress;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 最后连接时间
     */
    private LocalDateTime lastConnectedTime;

    /**
     * 最后回答时间
     */
    private LocalDateTime lastWorkingTime;

    /**
     * 最后断开连接时间
     */
    private LocalDateTime lastDisconnectedTime;

    /**
     * 最后登出时间
     */
    private LocalDateTime lastLogoutTime;

    /**
     * 最后连接错误时间
     */
    private LocalDateTime lastTransportErrorTime;

    private List<String> lastScreenImgPaths = Lists.newArrayList();

    private List<String> lastLogFilePaths = Lists.newArrayList();

    private String bidAccountNo;

    private String bidAccountPswd;

    private String bidAccountIdCard;

    private String bidStrategies;

    private String loginCoords;


    private int phase2StartSec;

    private List<PriceActionRequest> phase2Actions;// = Lists.newArrayList();

    private String seatNo;

    private int screenModeVal;

    public String id() {
        return machineCode;
    }

    public void addScreenImg(String imgPath) {
        last10(imgPath, lastScreenImgPaths);
    }

    public void addLogFile(String logFilePath) {
        last10(logFilePath, lastLogFilePaths);
    }

    private void last10(String path, List<String> paths) {
        if (StringUtils.isEmpty(path)) {
            return;
        }

        paths.add(path);

        if (paths.size() > 10) {
            paths.remove(0);
        }
    }

}
