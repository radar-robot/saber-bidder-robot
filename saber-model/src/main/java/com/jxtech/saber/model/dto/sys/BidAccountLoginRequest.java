package com.jxtech.saber.model.dto.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jxtech.saber.model.dto.BaseCommandRequest;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.model.enums.ImageScreenMode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * 登录 竞拍账号
 * Created by cook on 2019/7/13
 */
@Getter
@Setter
public class BidAccountLoginRequest extends BaseCommandRequest {

    // private String machineCode;

    private String bidAccountNo;

    private String bidAccountPswd;

    private String bidAccountIdCard;

    private String loginCoords;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private ImageScreenMode screenMode;

    private int screenModeVal;


    @JsonIgnore
    public ImageScreenMode getScreenMode() {
        return ImageScreenMode.valueOf(screenModeVal);
    }

    @JsonIgnore
    public void setScreenMode(ImageScreenMode screenMode) {
        this.screenModeVal = screenMode.getCode();
    }

}
