package com.jxtech.saber.model.dto.sys;

import com.jxtech.saber.model.dto.BaseCommandRequest;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/6/9
 */
@Getter
@Setter
public class ArgsCommandRequest extends BaseCommandRequest {

    private String args;

}
