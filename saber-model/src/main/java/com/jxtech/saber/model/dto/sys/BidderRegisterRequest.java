package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/3/31
 */
@Getter
@Setter
public class BidderRegisterRequest {

    /**
     * 机器IP
     */
    private String machineAddress;

    /**
     * 机器唯一地址
     */
    private String machineCode;

    private String localIpAddress;

    private String clientVersion;

    private String seatNo;

}
