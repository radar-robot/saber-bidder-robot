package com.jxtech.saber.model.bo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by cook on 2019/9/14
 */
@Getter
@Setter
public class PagePrice {

    private LocalDateTime screenTime;

    private int basePrice;

    public PagePrice() {
    }

    public PagePrice(LocalDateTime screenTime, int basePrice) {
        this.screenTime = screenTime;
        this.basePrice = basePrice;
    }
}
