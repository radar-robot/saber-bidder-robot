package com.jxtech.saber.model.dto.sys;

import com.jxtech.saber.model.dto.BaseCommandRequest;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/8/11
 */
@Getter
@Setter
public class LeftClickCommandRequest extends BaseCommandRequest {

    private String coord;

    private int screenModeVal;

    private String args;

}
