package com.jxtech.saber.model.enums;

import com.fxtech.panda.core.enums.CodeDescriptionFeature;
import com.fxtech.panda.core.enums.EnumHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 竞拍机器人 - 状态
 * Created by cook on 2019/3/31
 */
@Getter
@AllArgsConstructor
public enum BidderStatus implements CodeDescriptionFeature {

    CONNECTED(1, "已连接"),

    WORKING(2, "工作中"),

    OFFLINE(3, "下线"),

    NA(10, "未知 - 默认"),

    ;

    private int code;

    private String description;


    public static BidderStatus valueOf(Integer code) {
        return EnumHelper.valueOf(code, BidderStatus.class);
    }

}
