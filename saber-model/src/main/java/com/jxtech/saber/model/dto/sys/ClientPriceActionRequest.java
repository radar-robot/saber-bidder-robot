package com.jxtech.saber.model.dto.sys;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by cook on 2019/7/16
 */
@Getter
@Setter
public class ClientPriceActionRequest {

    private int startSec;

    private List<PriceActionRequest> actions;


}
