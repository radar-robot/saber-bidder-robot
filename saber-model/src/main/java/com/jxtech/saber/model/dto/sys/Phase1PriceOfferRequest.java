package com.jxtech.saber.model.dto.sys;

import com.jxtech.saber.model.dto.BaseCommandRequest;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by cook on 2019/7/13
 */
@Getter
@Setter
public class Phase1PriceOfferRequest extends BaseCommandRequest {

    // private String machineCode;

    private int price;

}
