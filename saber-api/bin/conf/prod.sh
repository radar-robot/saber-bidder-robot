#!/bin/sh
#export JAVA_HOME="/usr/java/default"
export JAVA_OPTS="-Dspring.profiles.active=prod -server -Djava.awt.headless=true -Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8 -XX:MetaspaceSize=256m -Xms3g -Xmx3g -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=0 -XX:+CMSParallelRemarkEnabled -XX:CMSInitiatingOccupancyFraction=80 -XX:+UseCMSInitiatingOccupancyOnly -XX:+PrintCommandLineFlags -Xloggc:/data/logs/saber-api/gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/logs/saber-api/java.hprof -Dio.netty.leakDetectionLevel=advanced"
export PROFILE="prod"
