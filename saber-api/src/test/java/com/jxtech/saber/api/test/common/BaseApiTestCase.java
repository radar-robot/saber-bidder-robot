package com.jxtech.saber.api.test.common;

import com.jxtech.saber.api.main.ApplicationStart;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by cook on 2018/9/26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStart.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseApiTestCase {

    protected static final Logger logger = LoggerFactory.getLogger(BaseApiTestCase.class);

    @Autowired
    protected TestRestTemplate restTemplate;

//    @Autowired
//    protected MockMvc mockMvc;

    @Value("${server.port:}")
    protected Integer serverPort;

    @Value("${local.server.port:}")
    protected Integer localServerPort;

}
