package com.jxtech.saber.api.main;

import com.fxtech.panda.core.utils.PrintUtils;
import com.fxtech.panda.spring.core.SmartApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication(scanBasePackages="com.jxtech.saber")
@PropertySource({"classpath:${spring.profiles.active}/application.properties", "classpath:application.properties"})
@PropertySource(value = "classpath:local/local.properties", ignoreResourceNotFound = true)
public class ApplicationStart {

    static {
        PrintUtils.disableStatusLogger();
    }

    public static void main(String[] args) {
        SmartApplication.start(ApplicationStart.class, args);
    }

}
