package com.jxtech.saber.api.interceptor;


import com.jxtech.saber.service.bizs.sys.AccountUserManager;
import com.jxtech.saber.support.common.RequireLogin;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * Created by cook on 2019/3/27
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

    @Autowired
    private AccountUserManager accountUserManager;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        RequireLogin anno = ((HandlerMethod) handler).getMethodAnnotation(RequireLogin.class);
        if (anno == null || !anno.value()) {
            return true;
        }

        boolean authed = checkAuth(request);

        if (!authed) {
            logger.info("authed false, need re-login");
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.sendRedirect("/saber/account/login");
            return false;
        }

        String jxValue = getJxValue(request);

        request.getSession().setAttribute("accountName", jxValue != null ? jxValue.split("_")[0] : "NA");

        return true;
    }

    private boolean checkAuth(HttpServletRequest request) {
        String jxValue = getJxValue(request);
        if (StringUtils.isEmpty(jxValue)) {
            return false;
        }

        String[] arr = jxValue.split("_");
        String username = arr[0];
        String auth = arr[1];

        String principle = accountUserManager.getAccountPrinciple(username);

        if (StringUtils.isNotEmpty(auth) && auth.equals(principle)) {
            return true;
        }

        return false;
    }

    private String getJxValue(HttpServletRequest request) {
        if (ArrayUtils.isEmpty(request.getCookies())) {
            return null;
        }

        Cookie ck = Arrays.stream(request.getCookies())
                .filter(x -> x.getName().equals("_saber_jx"))
                .findFirst()
                .orElse(null);
        if (ck == null) {
            return null;
        }

        return ck.getValue();
    }

}
