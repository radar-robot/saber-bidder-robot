package com.jxtech.saber.api.controller.sys;

import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.utils.DataResults;
import com.google.common.collect.Lists;
import com.jxtech.saber.model.dto.sys.BidderClient;
import com.jxtech.saber.model.dto.sys.ScreenImageUploadRequest;
import com.jxtech.saber.model.dto.sys.ScreenImageUploadResponse;
import com.jxtech.saber.properties.ProjectConfig;
import com.jxtech.saber.service.bizs.sys.AccountUserManager;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.support.utils.BizAssert;
import com.jxtech.saber.support.utils.K;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private ProjectConfig conf;

    @Autowired
    private AccountUserManager accountUserManager;

    @Autowired
    private RadarClientManager radarClientManager;

    @GetMapping("/account/login")
    public String accountLogin() {
        return "login";
    }

    @PostMapping("/account/do-login")
    public String accountLogin(@RequestParam String username,
                               @RequestParam String password,
                               Model model,
                               HttpServletResponse response,
                               HttpServletRequest request) {

        String pass = conf.getAdminAccounts().get(username);
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(pass)
            || !pass.equals(password)) {
            model.addAttribute("error", "账号或密码错误");
            return "register";
        }

        String principle = K.uuid();

        accountUserManager.setAccountPrinciple(username, principle);

        Cookie ck = new Cookie("_saber_jx", username + "_" + principle);
        ck.setPath("/");
        ck.setMaxAge(60 * 60);
        ck.setDomain(request.getServerName());

        response.addCookie(ck);

        model.addAttribute("accountName", username);

        return "redirect:/saber/admin";
    }


    @RequestMapping(value = "/v1/screen/upload", method = RequestMethod.POST)
    @ResponseBody
    public DataResult<ScreenImageUploadResponse> uploadCaptchaImage(ScreenImageUploadRequest q, StandardMultipartHttpServletRequest request) {
        //

        List<MultipartFile> files = request.getFileMap().entrySet()
                .stream()
                .filter(x -> StringUtils.isNotEmpty(x.getValue().getOriginalFilename()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        BizAssert.isTrue(CollectionUtils.isNotEmpty(files), "必须上传图片文件");

        String dt = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        String fileType = q.getUploadType() == 10 ? "log" : "image";

        ScreenImageUploadResponse response = new ScreenImageUploadResponse();

        String dir = conf.getScreenImagesDir() + "/" + dt + "/" + q.getMachineCode();// + "/" + q.getUid();
        Path dirPath = Paths.get(dir);
        if (Files.notExists(dirPath)) {
            try {
                Files.createDirectories(dirPath);
            } catch (IOException e) {
                throw new RuntimeException("创建目录失败", e);
            }
        }

        List<String> fileUris = Lists.newArrayList();
        BidderClient client = radarClientManager.getByMachineCode(q.getMachineCode());
        AtomicInteger fileCount = new AtomicInteger();

        files.forEach(file -> {
            if (StringUtils.isEmpty(file.getOriginalFilename())) {
                return;
            }

            // 中文存在乱码问题, 这里文件名不重要, 替换
//            String fName = new String(Objects.requireNonNull(file.getOriginalFilename()).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//            String fName = file.getOriginalFilename();
            // TODO: 这里可以进行适度压缩, 减少文件大小
            int idx = file.getOriginalFilename().lastIndexOf('.');
            String suffix = file.getOriginalFilename().substring(idx + 1);
            // String fName = "file-" + fileCount.getAndIncrement() + "." + suffix;
            String fName = file.getOriginalFilename();
            Path filePath = Paths.get(dir + "/" + fName);
            logger.info("create {} filePath is {}", fileType, filePath);
            try {
                Files.deleteIfExists(filePath);
                Files.createFile(filePath);
            } catch (IOException e) {
                logger.error("", e);
                throw new RuntimeException("create file " + filePath + " error", e);
            }

            try (FileOutputStream fos = new FileOutputStream(filePath.toFile())) {
                IOUtils.copyLarge(file.getInputStream(), fos);
                // TODO: 这里可以把图片用nginx代理, 减少服务端的请求压力
                //  + "/" + q.getUid()
                String fileUri = conf.getScreenImageAddressPrefix() + "" + conf.getScreenImageUrlPrefix() + "/" + dt + "/" + q.getMachineCode()+ "/" + fName;

                fileUris.add(fileUri);

                if (q.getUploadType() == 10) {
                    client.addLogFile(fileUri);
                } else {
                    client.addScreenImg(fileUri);
                }

            } catch (IOException e) {
                logger.error("", e);
                throw new RuntimeException("copy file " + filePath + " error", e);
            }
        });

        response.setFilePaths(fileUris);

        return DataResults.ok(response);
    }

}
