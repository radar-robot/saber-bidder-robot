package com.jxtech.saber.api.config;

import com.jxtech.saber.support.exception.BizException;
import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.utils.DataResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@ControllerAdvice
@ResponseBody
public class WebControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(WebControllerAdvice.class);

    /**
     * 处理业务异常, 返回json
     * @param ex
     * @return
     */
    @ExceptionHandler({BizException.class})
    public DataResult handleBizException(BizException ex) {
        return DataResults.fail(ex.getErrorCode(), ex.getMessage());
    }

}
