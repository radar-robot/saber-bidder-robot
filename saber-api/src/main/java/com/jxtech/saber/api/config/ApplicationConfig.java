package com.jxtech.saber.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.fxtech.panda.core.json.Jack;
import com.jxtech.saber.api.interceptor.LoginInterceptor;
import com.jxtech.saber.properties.ProjectConfig;
import com.jxtech.saber.service.socket.netty.NettySocketServer;
import com.jxtech.saber.support.json.CustomEnumDeserializers;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目的一些配置
 */
@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private ProjectConfig conf;

    @Autowired
    private NettySocketServer socketServer;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/RadarBidClient/**")
                .addResourceLocations(
                        conf.getClientUpdateLocation())
                .setCachePeriod(15 * 60)
        ;

        logger.info("ClientUpdateLocation is {}, ScreenImagesResourcePath is {}", conf.getClientUpdateLocation(), conf.getScreenImagesResourcePath());

        registry.addResourceHandler("/images/**")
                .addResourceLocations(
                        conf.getScreenImagesResourcePath())
                .setCachePeriod(2 * 60 * 60)
        ;

    }

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor());
        logger.info("add interceptors ...");
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            logger.info("using active profile is {}", activeProfile);
            logger.info("inspect the beans provided by Spring Boot, total {} beans", ctx.getBeanDefinitionNames().length);

            if (!Files.exists(Paths.get(conf.getScreenImagesDir()))) {
                Files.createDirectories(Paths.get(conf.getScreenImagesDir()));
            }

            socketServer.start();

        };
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

        for (HttpMessageConverter<?> conv : new ArrayList<>(converters)) {
            if (conv instanceof MappingJackson2HttpMessageConverter) {
                converters.remove(conv);
                logger.info("remove conv#{}", conv);
            }
        }

        converters.add(customJackson2HttpMessageConverter());
    }

    @Bean
    public MappingJackson2HttpMessageConverter customJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = Jack.createMapper();

        SimpleModule enumModule = new SimpleModule();
        enumModule.setDeserializers(new CustomEnumDeserializers());

        objectMapper.registerModule(enumModule);

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")));
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));

        objectMapper.registerModule(javaTimeModule);

        jsonConverter.setObjectMapper(objectMapper);
        return jsonConverter;
    }
}
