package com.jxtech.saber.api.controller.sys;

import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.utils.DataResults;
import com.jxtech.saber.model.dto.sys.*;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.service.bizs.sys.BidderCommandService;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.support.common.RequireLogin;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by cook on 2019/7/12
 */
@Controller
public class WorkbenchController {

    @Autowired
    private RadarClientManager bidderUserManager;

    @Autowired
    private BidderCommandService commandService;


    @GetMapping("/workbench")
    @RequireLogin
    public ModelAndView workbench(@RequestParam String machineCode) {
        ModelAndView mav = new ModelAndView("workbench");
        mav.addObject("client", bidderUserManager.getByMachineCode(machineCode));

        return mav;
    }

    @ApiOperation(tags = "Workbench", value = "GetClient", httpMethod = "GET",
            response = DataResult.class, notes = "GetClient")
    @GetMapping("/v1/workbench/client/get")
    @ResponseBody
    @RequireLogin
    public DataResult<BidderClient> getClient(@RequestParam String machineCode) {
        BidderClient client = bidderUserManager.getByMachineCode(machineCode);
        return DataResults.ok(client);
    }

    @ApiOperation(tags = "Workbench", value = "loginClient", httpMethod = "POST",
            response = DataResult.class, notes = "loginClient")
    @PostMapping("/v1/workbench/client/login")
    @ResponseBody
    @RequireLogin
    public DataResult<String> loginClient(@RequestBody BidderClient r) {
        BidderClient client = bidderUserManager.getByMachineCode(r.getMachineCode());

        client.setBidAccountNo(r.getBidAccountNo());
        client.setBidAccountPswd(r.getBidAccountPswd());
        client.setBidAccountIdCard(r.getBidAccountIdCard());

        BidAccountLoginRequest req = new BidAccountLoginRequest();

        req.setMachineCode(r.getMachineCode());
        req.setBidAccountNo(r.getBidAccountNo());
        req.setBidAccountPswd(r.getBidAccountPswd());
        req.setBidAccountIdCard(r.getBidAccountIdCard());
        req.setLoginCoords(r.getLoginCoords());

        req.setClientNo(client.getAssignedClientNo());
        req.setDirective(CommandDirective.BID_ACCOUNT_LOGIN);
        req.setScreenModeVal(r.getScreenModeVal());


        DataResult<String> dr = commandService.bidAccountLogin(req);

        return dr;
    }

    @ApiOperation(tags = "Workbench", value = "leftClick", httpMethod = "POST",
            response = DataResult.class, notes = "leftClick")
    @PostMapping("/v1/workbench/client/left-click")
    @ResponseBody
    @RequireLogin
    public DataResult<String> leftClick(@RequestBody LeftClickCommandRequest req) {
        BidderClient client = bidderUserManager.getByMachineCode(req.getMachineCode());

        req.setClientNo(client.getAssignedClientNo());

        DataResult<String> dr = commandService.leftClick(req);

        return dr;
    }

    @ApiOperation(tags = "Workbench", value = "inputText", httpMethod = "POST",
            response = DataResult.class, notes = "inputText")
    @PostMapping("/v1/workbench/client/input-text")
    @ResponseBody
    @RequireLogin
    public DataResult<String> inputText(@RequestBody InputTextCommandRequest req) {
        BidderClient client = bidderUserManager.getByMachineCode(req.getMachineCode());

        req.setClientNo(client.getAssignedClientNo());

        DataResult<String> dr = commandService.inputText(req);

        return dr;
    }

    @ApiOperation(tags = "Workbench", value = "offerPrice4Phase1", httpMethod = "POST",
            response = DataResult.class, notes = "offerPrice4Phase1")
    @PostMapping("/v1/workbench/client/offer-price-phase1")
    @ResponseBody
    @RequireLogin
    public DataResult<String> offerPrice4Phase1(@RequestBody Phase1PriceOfferRequest r) {
        BidderClient client = bidderUserManager.getByMachineCode(r.getMachineCode());

        Phase1PriceOfferRequest req = new Phase1PriceOfferRequest();
        req.setClientNo(client.getAssignedClientNo());
        req.setDirective(CommandDirective.PHASE1_PRICE_OFFER);

        req.setMachineCode(client.getMachineCode());
        req.setPrice(r.getPrice());


        DataResult<String> dr = commandService.phase1PriceOffer(req);

        return dr;
    }

    @ApiOperation(tags = "Workbench", value = "submitPrice4Phase1", httpMethod = "POST",
            response = DataResult.class, notes = "submitPrice4Phase1")
    @PostMapping("/v1/workbench/client/submit-price-phase1")
    @ResponseBody
    @RequireLogin
    public DataResult<String> submitPrice4Phase1(@RequestBody Phase1PriceSubmitRequest r) {
        BidderClient client = bidderUserManager.getByMachineCode(r.getMachineCode());

        Phase1PriceSubmitRequest req = new Phase1PriceSubmitRequest();
        req.setClientNo(client.getAssignedClientNo());
        req.setDirective(CommandDirective.PHASE1_PRICE_SUBMIT);

        req.setMachineCode(client.getMachineCode());
        req.setAnswer(r.getAnswer());


        DataResult<String> dr = commandService.phase1PriceSubmit(req);

        return dr;
    }

    @ApiOperation(tags = "Workbench", value = "setBidStrategies", httpMethod = "POST",
            response = DataResult.class, notes = "setBidStrategies")
    @PostMapping("/v1/workbench/client/set-bid-strategies")
    @ResponseBody
    @RequireLogin
    public DataResult<String> setBidStrategies(@RequestBody BidStrategiesSetRequest r) {
        BidderClient client = bidderUserManager.getByMachineCode(r.getMachineCode());

        client.setBidStrategies(r.getBidStrategies());

        BidStrategiesSetRequest req = new BidStrategiesSetRequest();
        req.setClientNo(client.getAssignedClientNo());
        req.setDirective(CommandDirective.BID_STRATEGIES_SET);

        req.setMachineCode(client.getMachineCode());
        req.setBidStrategies(r.getBidStrategies());


        DataResult<String> dr = commandService.setBidStrategies(req);

        return dr;
    }

}
