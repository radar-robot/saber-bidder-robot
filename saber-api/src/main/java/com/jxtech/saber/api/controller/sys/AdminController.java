package com.jxtech.saber.api.controller.sys;

import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.json.Jack;
import com.fxtech.panda.core.utils.DataResults;
import com.fxtech.panda.core.utils.TextUtils;
import com.jxtech.saber.model.bo.AccountSetting;
import com.jxtech.saber.model.dto.sys.*;
import com.jxtech.saber.model.enums.CommandDirective;
import com.jxtech.saber.service.bizs.sys.BidderCommandService;
import com.jxtech.saber.service.bizs.sys.RadarClientManager;
import com.jxtech.saber.service.bizs.sys.UserChannelManager;
import com.jxtech.saber.service.services.sys.AccountSettingService;
import com.jxtech.saber.support.common.FrontPage;
import com.jxtech.saber.support.common.FrontPageResult;
import com.jxtech.saber.support.common.RequireLogin;
import com.jxtech.saber.support.utils.K;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cook on 2019/3/31
 */
@Controller
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UserChannelManager userChannelManager;

    @Autowired
    private RadarClientManager bidderUserManager;

    @Autowired
    private BidderCommandService commandService;

    @Autowired
    private RadarClientManager radarClientManager;

    @Autowired
    private AccountSettingService accountSettingService;

    @PostMapping(value = "/v1/admin/capture-screen")
    @ResponseBody
    @RequireLogin
    public DataResult<String> captureScreen(@RequestBody BidderCaptureScreenRequest req) {
        DataResult<String> ret = commandService.captureScreen(req.getClientNo());
        return ret;
    }

    @PostMapping(value = "/v1/admin/args-command")
    @ResponseBody
    @RequireLogin
    public DataResult<String> argsCommand(@RequestBody ArgsCommandRequest req) {
        DataResult<String> ret = commandService.sendCommand(req.getClientNo(), req.getDirective(), req.getArgs());
        return ret;
    }

    @PostMapping(value = "/v1/admin/args-commands")
    @ResponseBody
    @RequireLogin
    public DataResult<String> batchArgsCommand(@RequestBody BatchArgsCommandRequest req) {
        List<Integer> nos = TextUtils.splitClean(req.getClientNos())
                .stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        return commandService.batchSendCommand(nos, req.getDirective(), req.getArgs());
    }

    @ApiOperation(tags = "Admin", value = "QueryBees", httpMethod = "POST",
            response = DataResult.class, notes = "QueryBees")
    @GetMapping("/v1/admin/query-bidder-clients")
    @ResponseBody
    @RequireLogin
    public FrontPageResult queryBidders(HttpServletRequest request) {
        FrontPage fpage = K.convert(request);

        List<BidderClient> bidders = bidderUserManager.queryAllBidders();
        bidders.sort(Comparator.comparingInt(BidderClient::getAssignedClientNo));

        FrontPageResult result = new FrontPageResult();
        result.setRows(bidders);
        result.setTotal(bidders.size());

        return result;
    }

    @GetMapping("/admin")
    @RequireLogin
    public String adminPage() {

        return "admin";
    }

    //
    @GetMapping("/v1/bidder/screens")
    @ResponseBody
    @RequireLogin
    public DataResult<List<String>> getBidderScreenImgs(@RequestParam int clientNo) {
        BidderClient client = radarClientManager.getByClientNo(clientNo);
        return DataResults.ok(client.getLastScreenImgPaths());
    }


    @PostMapping(value = "/v1/admin/upload-account-setting")
    @ResponseBody
    @RequireLogin
    public DataResult<String> uploadAccountSetting(@RequestParam("account_setting") MultipartFile file) {
        try {
            List<String> lines = IOUtils.readLines(file.getInputStream(), "UTF-8");
            int total = 0;

            for (String line : lines) {
                if (StringUtils.isBlank(line) || line.startsWith("#")) {
                    continue;
                }

                // #机器码;竞拍账号;密码;身份证;竞拍策略
                String[] arr = line.split(";");
                AccountSetting setting = new AccountSetting();
                setting.setMachineCode(arr[0]);
                setting.setAccountNo(arr[1]);
                setting.setAccountPswd(arr[2]);
                setting.setIdCard(arr[3]);
                setting.setSubmitStrategy(arr[4]);

                accountSettingService.addAccountSetting(setting);
                total++;
            }

            return DataResults.ok("共载入 " + total + " 条");
        } catch (IOException e) {
            throw new RuntimeException("uploadAccountSetting error", e);
        }
    }


    @PostMapping(value = "/v1/admin/bind-account-setting")
    @ResponseBody
    @RequireLogin
    public DataResult<String> bindAccountSetting() {
        int total = 0;
        List<BidderClient> bidders = bidderUserManager.queryAllBidders();
        for (BidderClient bidder : bidders) {
            AccountSetting setting = accountSettingService.getByMachineCode(bidder.getMachineCode());
            if (setting == null) {
                continue;
            }

            logger.info("start bind bidder#{} with setting#{}", bidder.getMachineCode(), setting.toText());

            bidder.setBidAccountIdCard(setting.getIdCard());
            bidder.setBidAccountNo(setting.getAccountNo());
            bidder.setBidAccountPswd(setting.getAccountPswd());
            bidder.setBidAccountIdCard(setting.getIdCard());

            String strategy = setting.getSubmitStrategy().replace('|', '\n');

            bidder.setBidStrategies(strategy);

            BidStrategiesSetRequest req = new BidStrategiesSetRequest();
            req.setBidStrategies(strategy);
            req.setClientNo(bidder.getAssignedClientNo());
            req.setDirective(CommandDirective.BID_STRATEGIES_SET);
            req.setMachineCode(bidder.getMachineCode());

            DataResult<String> commRet = commandService.sendCommand(bidder.getAssignedClientNo(),
                    CommandDirective.BID_STRATEGIES_SET, req);
            logger.info("BID_STRATEGIES_SET result is {}", Jack.toJson(commRet));

            total++;
        }

        return DataResults.ok("共绑定 " + total + " 条");
    }

}
