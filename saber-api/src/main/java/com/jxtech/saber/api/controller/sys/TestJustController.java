package com.jxtech.saber.api.controller.sys;

import com.jxtech.saber.model.dto.sys.TestJustResponse;
import com.jxtech.saber.service.bizs.sys.TestJustBiz;
import com.fxtech.panda.core.common.DataResult;
import com.fxtech.panda.core.utils.DataResults;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by cook on 2019/1/24
 */
@RestController
@Api(tags = "TEST")
@RequestMapping(value="/v1/test")
public class TestJustController {

    @Autowired
    private TestJustBiz testBiz;

    @ApiOperation(tags = "TEST", value = "Just", httpMethod = "GET",
            response = DataResult.class, notes = "测试")
    @GetMapping("/just")
    public DataResult<List<TestJustResponse>> just(@RequestParam("userId") Integer userId) {
        List<TestJustResponse> just = testBiz.findJust(userId);
        return DataResults.ok(just);
    }


}
