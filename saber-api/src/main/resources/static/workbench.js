
var lastCoords = [];

var lastScreenMode = 1;

function fetchLastScreenImg(isAlert) {
    var machineCode = $('#machineCode').val();
    getRequest('/saber/v1/workbench/client/get?machineCode=' + machineCode, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            var client = ret.data;
            var imgPaths = client.lastScreenImgPaths;
            if (imgPaths != null && imgPaths.length > 0) {
                var lastPath = imgPaths[imgPaths.length - 1];
                if (lastPath.indexOf('full-screen') > 0) {
                    lastScreenMode = 10;
                } else if (lastPath.indexOf('flash-screen') > 0) {
                    lastScreenMode = 1;
                } else {
                    lastScreenMode = 1;
                }
                // height="700" width="900"
                $('#lastScreen').html('<img id="loadedScreenImg" src="' + lastPath + '"   alt="dragon">');
                $('#loadedScreenImg').on('click', imgClickCoord)
            } else {
                if (isAlert !== false) {
                    alert("成功 - " + "no images");
                }

            }
        } else {
            if (isAlert !== false) {
                alert("失败 - " + ret.message);
            }

        }
    });
}

function imgClickCoord(event) {
    var parentOffset = $(this).parent().offset();
    //or $(this).offset(); if you really just want the current element's offset

    var x = event.pageX - parentOffset.left;
    var y = event.pageY - parentOffset.top;
    var coord = x + "," + y;
    lastCoords.push(coord);
    $('#miscCoord').val(coord);

    if (lastCoords.length > 3) {
        lastCoords.splice(0, 1);
    }

    $('#loginCoords').val(lastCoords.join(";"));

    console.log("X Coordinate: " + x + " Y Coordinate: " + y + ". lastCoords is " + lastCoords);
}

function uploadBidScreen2(directiveVal) {
    var assignedClientNo = $('#assignedClientNo').val();
    uploadBidScreen(assignedClientNo, directiveVal)
}

function uploadBidScreen(clientNo, directiveVal) {
    var reqData = JSON.stringify({clientNo: clientNo, directiveVal: directiveVal, args: ''});

    postRequest('/saber/v1/admin/args-command', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });
}

function getBidScreens2() {
    var assignedClientNo = $('#assignedClientNo').val();
    getBidScreens(assignedClientNo);
}

function getBidScreens(clientNo) {
    // http://127.0.0.1:8877/v1/bidder/screens?clientNo=13
    // {"status":0,"data":["/images/f448c6592279116a695aaa920a5b9a8c/5039a90bafff4705ba65035a79d07a1e-234925-screen.jpg"],"message":""}
    getRequest('/saber/v1/bidder/screens?clientNo=' + clientNo, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            var imgPaths = ret.data;
            if (imgPaths != null && imgPaths.length > 0) {
                var lastPath = imgPaths[imgPaths.length - 1];
                alert("成功 - " + "<img src='" + lastPath + "' >");
            } else {
                alert("成功 - " + "no images");
            }
        } else {
            alert("失败 - " + ret.message);
        }
    });
}

function loginBidAccount() {

    if (lastCoords.length < 3) {
        alert("输入验证码");
        return;
    }

    var machineCode = $('#machineCode').val();
    var bindBidAccountNo = $('#bidAccountNo').val();
    var bindBidAccountPswd = $('#bidAccountPswd').val();
    var bindBidAccountIdCard = $('#bidAccountIdCard').val();
    var loginCoords = $('#loginCoords').val();

    var reqData = {
        machineCode: machineCode,
        bidAccountNo: bindBidAccountNo,
        bidAccountPswd: bindBidAccountPswd,
        bidAccountIdCard: bindBidAccountIdCard,
        loginCoords: loginCoords,
        screenModeVal: lastScreenMode
    };

    postRequest('/saber/v1/workbench/client/login', 'json', JSON.stringify(reqData), function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });

}

function offerPrice4Phase1() {
    var priceOfPhase1 = $('#priceOfPhase1').val();
    if (priceOfPhase1 == null || priceOfPhase1 == '') {
        alert("输入第一阶段价格");
        return;
    }

    var machineCode = $('#machineCode').val();

    var reqData = {
        machineCode: machineCode,
        price: priceOfPhase1
    };

    postRequest('/saber/v1/workbench/client/offer-price-phase1', 'json', JSON.stringify(reqData), function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });

}

function submitPrice4Phase1() {
    var answer = $('#answerOfPhase1').val();
    if (answer == null || answer == '') {
        alert("输入第一阶段验证码");
        return;
    }

    var machineCode = $('#machineCode').val();

    var reqData = {
        machineCode: machineCode,
        answer: answer
    };

    postRequest('/saber/v1/workbench/client/submit-price-phase1', 'json', JSON.stringify(reqData), function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });

}

function setBidStrategies() {
    var bidStrategies = $('#bidStrategies').val();
    if (bidStrategies == null || bidStrategies == '') {
        alert("输入竞拍策略");
        return;
    }

    var machineCode = $('#machineCode').val();

    var reqData = {
        machineCode: machineCode,
        bidStrategies: bidStrategies
    };

    postRequest('/saber/v1/workbench/client/set-bid-strategies', 'json', JSON.stringify(reqData), function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });

}

function controlClick() {
    var coord = $('#miscCoord').val();
    if (coord.length < 1) {
        alert("输入操作坐标");
        return;
    }

    var machineCode = $('#machineCode').val();
    var reqData = JSON.stringify({coord: coord, screenModeVal: lastScreenMode, directiveVal: 90301,
        machineCode: machineCode, args: ''});

    postRequest('/saber/v1/workbench/client/left-click', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });
}

function controlInput() {
    var coord = $('#miscCoord').val();
    if (coord.length < 1) {
        alert("输入操作坐标");
        return;
    }

    var operateText = $('#operateText').val();
    if (operateText.length < 1) {
        alert("输入操作文本");
        return;
    }

    var machineCode = $('#machineCode').val();
    var reqData = JSON.stringify({coord: coord, screenModeVal: lastScreenMode, directiveVal: 90302,
        machineCode: machineCode, text: operateText, args: ''});


    postRequest('/saber/v1/workbench/client/input-text', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });

}

function uploadLogFile() {
    var clientNo = $('#assignedClientNo').val();
    var reqData = JSON.stringify({clientNo: clientNo, directiveVal: 90210, args: ''});

    postRequest('/saber/v1/admin/args-command', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    });
}

function showLogFileLink(isAlert) {
    var machineCode = $('#machineCode').val();
    getRequest('/saber/v1/workbench/client/get?machineCode=' + machineCode, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            var client = ret.data;
            var filePaths = client.lastLogFilePaths;
            if (filePaths != null && filePaths.length > 0) {
                var lastPath = filePaths[filePaths.length - 1];

                var fileName = lastPath.substring(lastPath.lastIndexOf('/')+1);;

                $('#lastLogFile').html('<a onclick="javascript:;" href="' + lastPath + '">' + fileName + '</a>');
            } else {
                if (isAlert !== false) {
                    alert("成功 - " + "no files");
                }
            }
        } else {
            if (isAlert !== false) {
                alert("失败 - " + ret.message);
            }

        }
    });
}

$( document ).ready(function() {
    fetchLastScreenImg(false);
});

