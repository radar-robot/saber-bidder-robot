
function currentTs() {
    return Math.floor(new Date().getTime() / 1000);
}

function currentMills() {
    return new Date().getTime();
}

function fromMills(mills) {
    return new Date(mills);
}

function formatDate(dt) {
    return dt == null ? "-" : dt.format("yyyy-MM-dd hh:mm:ss");
}

function postRequest(url, dataType, data, successCallback, errorCallback) {
    var opts = {
        url: url,
        type: 'POST',
        data: data,
        success: function (data, textStatus, jqXHR) {
            if (successCallback) {
                successCallback(data, textStatus, jqXHR);
            } else {
                console.log("successCallback is not defined");
            }

        },
        error: function (jqXHR, textStatus, errorThrown ) {
            if (errorCallback) {
                errorCallback(jqXHR, textStatus, errorThrown);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    };

    if ("json" == dataType) {
        opts['contentType'] = "application/json; charset=utf-8";
        opts['dataType'] = "json";
    }

    $.ajax(opts);
}

function getRequest(url, successCallback, errorCallback) {
    var opts = {
        url: url,
        type: 'GET',
        success: function (data, textStatus, jqXHR) {
            if (successCallback) {
                successCallback(data, textStatus, jqXHR);
            } else {
                console.log("successCallback is not defined");
            }

        },
        error: function (jqXHR, textStatus, errorThrown ) {
            if (errorCallback) {
                errorCallback(jqXHR, textStatus, errorThrown);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    };

    $.ajax(opts);
}

Date.prototype.format = function(fmt) {
    var o = {
        "M+" : this.getMonth()+1, //月份
        "d+" : this.getDate(), //日
        "h+" : this.getHours(), //小时
        "m+" : this.getMinutes(), //分
        "s+" : this.getSeconds(), //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S" : this.getMilliseconds() //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
}

