
var tbl = $('#tbl');

function captureScreen() {

    // $('#table').bootstrapTable('method', parameter).

    var arr = tbl.bootstrapTable('getSelections');

    // for

    var reqData = JSON.stringify({machineCode: 'f448c6592279116a695aaa920a5b9a8c'});

    postRequest('/saber/v1/admin/capture-screen', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
           alert("成功 - " + ret.data);
        } else {
           alert("失败 - " + ret.message);
        }
    })
}


function operateFormatter(value, row, index) {
    return [
        '<div class="left">',
        '<a href="/saber/workbench?machineCode=' + row.machineCode + '" target="_blank" >工作台</a>',
        '</div>'
    ].join('')
}

function bidStrategiesFormatter(value, row, index) {
    var regex = /\n/gi;
    return value == null ? '' : value.replace(regex, '<br>');
}

function phase2ActionsFormatter(value, row, index) {
    var acts = row.phase2Actions;
    if (acts === null || acts.length == 0) {
        return 'NA';
    }

    var headerCols = [];
    var ro = [];
    var lines = [];
    for(var j = 0; j < acts.length; j++) {
        var act = acts[j];
        var line = "<div style='width: 280px;'>";

        if (act == null) {
            continue;
        }

        line += act.occurTime.substring(11) + "|";

        if (act.action === 'PRICE_OFFER') {
            line += "出价";
        } else if (act.action === 'PRICE_SUBMIT') {
            line += "提交";
        } else {
            line += "NA";
        }

        line += "|";

        line += act.screenTime.substring(14) + "|";
        line += act.screenPrice + "|";
        line += act.targetPrice + "|";
        line += act.usedDelayMills + "|";

        line += (act.memo == null ? "" : act.memo);

        line += "</div>";

        lines.push(line);

        // headerCols.push(time.substr(17, 2));
        // ro.push(act.price);

        // var ret = act.screenTime.substring(11) + ", " + actName + ", " + act.price;
        // hs.push(ret);
    }

    return lines.join("");

    // var rows = [];
    // rows.push(ro);
    // return buildTable(headerCols, rows);
}

function buildTable(headerCols, rows) {


    var html = ['<table>'];

    // header
    html.push('<thead>');
    html.push('<tr>');
    for (var j=0; j<headerCols.length; j++) {
        var c1 = headerCols[j];
        html.push('<th>');
        html.push(c1);
        html.push('</th>');
    }
    html.push('</tr>');
    html.push('</thead>');

    // rows
    html.push('<tbody>');

    for (var m=0; m<rows.length; m++) {
        var rowCols = rows[m];
        html.push('<tr>');
        for (var i=0; i<rowCols.length; i++) {
            var c2 = rowCols[i];
            html.push('<td>');
            html.push(c2);
            html.push('</td>');
        }
        html.push('</tr>');
    }

    html.push('</tbody>');
    html.push('</table>');

    return html.join(' ');

}

function uploadAccountSetting() {
    var formData = new FormData();

    formData.append("account_setting",$('#account_setting')[0].files[0]);

    $.ajax({
        url:'/saber/v1/admin/upload-account-setting',
        dataType:'json',
        type:'POST',
        data: formData,
        processData : false, // 使数据不做处理
        contentType : false, // 不要设置Content-Type请求头
        success: function(ret){
            console.log(ret);
            if (ret.status == 0) {
                alert('处理成功, ' + ret.data);
            } else {
                alert('处理失败, ' + ret.message);
            }
        },
        error:function(response){
            alert('处理错误, ' + response);
        }
    });
}

function bindAccountSetting() {
    var reqData = {};

    postRequest('/saber/v1/admin/bind-account-setting', 'json', reqData, function(data) {
        var ret = data;
        if (ret && ret.status == 0 ) {
            alert("成功 - " + ret.data);
        } else {
            alert("失败 - " + ret.message);
        }
    })
}

$( document ).ready(function() {
       // $('#tbl').bootstrapTable('refresh');
});



